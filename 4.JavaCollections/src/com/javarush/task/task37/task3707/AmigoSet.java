package com.javarush.task.task37.task3707;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

public class AmigoSet<E> extends AbstractSet<E> implements Serializable, Cloneable, Set<E>{
    private static final Object PRESENT = new Object();
    private transient HashMap<E,Object> map;
    @Override
    public Iterator iterator() {
        return this.map.keySet().iterator();
    }

    public AmigoSet(Collection<? extends E> collection) {
        int capacity = (int) Math.ceil(Math.max(16, (collection.size()/.75f)));
        this.map = new HashMap<>(capacity);
        this.addAll(collection);
    }

    public AmigoSet() {
        this.map = new HashMap<>();
    }

    public boolean add(E e) {
        return null == map.put(e, PRESENT);
    }

    @Override
    public int size() {
        return this.map.size();
    }

    @Override
    public Object clone() {
        AmigoSet amigoSet = new AmigoSet();
        try {
            amigoSet.map = new HashMap<E,Object>();
            amigoSet.map.putAll((Map) this.map.clone());
            return amigoSet;
        } catch (Exception e) {
            throw new InternalError();
        }
    }

    @Override
    public boolean isEmpty() {
        return this.map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return this.map.keySet().contains(o);
    }

    @Override
    public boolean remove(Object o) {
        return this.map.remove(o) != null;
    }

    @Override
    public void clear() {
        this.map.clear();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        out.writeFloat(HashMapReflectionHelper.callHiddenMethod(map, "loadFactor"));
        out.writeInt(HashMapReflectionHelper.callHiddenMethod(map, "capacity"));
        out.writeInt(map.size());
        for (E e : map.keySet()
                ) {
            out.writeObject(e);
        }
    }
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        float loadFactor = in.readFloat();
        int capacity = in.readInt();
        map = new HashMap(capacity, loadFactor);
        int size = in.readInt();
        Set keys = new HashSet<>(size);
        for (int i = 0; i < size; i++) {
            keys.add(in.readObject());
        }
        //Set keys = (Set)in.readObject();
        addAll(keys);
    }
}

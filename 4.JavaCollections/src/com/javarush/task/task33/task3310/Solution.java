package com.javarush.task.task33.task3310;

import com.javarush.task.task33.task3310.strategy.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static Set<Long> getIds(Shortener shortener, Set<String> strings) {
        Set<Long> resultSet = new HashSet<Long>();
        for (String s :
                strings) {
            resultSet.add(shortener.getId(s));
        }
        return resultSet;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys) {
        Set<String> resultSet = new HashSet<String>();
        for (Long l :
                keys) {
            resultSet.add(shortener.getString(l));
        }
        return resultSet;
    }

    public static void testStrategy(StorageStrategy strategy, long elementsNumber) {
        Helper.printMessage(strategy.getClass().getSimpleName());
        Set<String> strings = new HashSet<>();
        for (int i = 0; i < elementsNumber; i++) {
            strings.add(Helper.generateRandomString());
        }
        Shortener shortener = new Shortener(strategy);
        Set<Long> longSet = new HashSet<>();
        Date startDate = new Date();
        longSet = getIds(shortener, strings);
        Date endDate = new Date();
        Long time = endDate.getTime() - startDate.getTime();
        Helper.printMessage(time.toString());
        Set<String> newStrings = new HashSet<>();
        startDate = new Date();
        newStrings = getStrings(shortener, longSet);
        endDate = new Date();
        time = endDate.getTime() - startDate.getTime();
        Helper.printMessage(time.toString());
        if (strings.equals(newStrings)) {
            Helper.printMessage("Тест пройден.");
        } else {
            Helper.printMessage("Тест не пройден.");
        }
    }

    public static void main(String[] args) {
        testStrategy(new DualHashBidiMapStorageStrategy(), 10);
    }

}

package com.javarush.task.task36.task3610;

import java.io.Serializable;
import java.util.*;

public class MyMultiMap<K, V> extends HashMap<K, V> implements Cloneable, Serializable {
    static final long serialVersionUID = 123456789L;
    private HashMap<K, List<V>> map;
    private int repeatCount;

    public MyMultiMap(int repeatCount) {
        this.repeatCount = repeatCount;
        map = new HashMap<>();
    }

    @Override
    public int size() {
        int size = 0;
        for (List<V> list :
                map.values()) {
            size += list.size();
        }
        return size;
        //напишите тут ваш код
    }

    @Override
    public V put(K key, V value) {
        List<V> list = null;
        V last = null;
        if (map.containsKey(key)) {
            list = map.get(key);
            last = list.get((list.size() - 1));
            if (list.size() == repeatCount) {
                list.remove(0);
            }
        } else {
            list = new ArrayList<V>();
            map.put(key, list);
        }
        list.add(value);
        return last;
        //напишите тут ваш код
    }

    @Override
    public V remove(Object key) {
        List<V> list = null;
        V last = null;
        if (map.containsKey(key)) {
            list = map.get(key);
            last = list.remove(0);
            if (list.size() == 0) {
                map.remove(key);
            }
        }
        return last;
        //напишите тут ваш код
    }

    @Override
    public Set<K> keySet() {
        return map.keySet();
        //напишите тут ваш код
    }

    @Override
    public Collection<V> values() {
        ArrayList<V> arrayList = new ArrayList<>();
        for (List<V> list :
                map.values()) {
            arrayList.addAll(list);
        }
        return arrayList;
        //напишите тут ваш код
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        for (List<V> list :
                map.values()) {
            if (list.contains(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        for (Map.Entry<K, List<V>> entry : map.entrySet()) {
            sb.append(entry.getKey());
            sb.append("=");
            for (V v : entry.getValue()) {
                sb.append(v);
                sb.append(", ");
            }
        }
        String substring = sb.substring(0, sb.length() - 2);
        return substring + "}";
    }
}
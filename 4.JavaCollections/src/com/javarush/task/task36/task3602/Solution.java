package com.javarush.task.task36.task3602;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.List;

/* 
Найти класс по описанию
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getExpectedClass());
    }

    public static Class getExpectedClass() {
        for (Class clazz : Collections.class.getDeclaredClasses()) {
            if (Modifier.isStatic(clazz.getModifiers()) && Modifier.isPrivate(clazz.getModifiers())) {
                Class supClazz = clazz;
                boolean listFound = false;
                do {
                    for (Class intrf : supClazz.getInterfaces()) {
                        if (intrf.getSimpleName().equals("List")) {
                            listFound = true;
                        }
                    }
                    supClazz = supClazz.getSuperclass();
                } while (!listFound && supClazz != null);
                if (listFound) {
                    try {
                        Constructor constructor = clazz.getDeclaredConstructor();
                        constructor.setAccessible(true);
                        List testList = (List) constructor.newInstance();
                        testList.get(0);
                    } catch (IndexOutOfBoundsException e) {
                        return clazz;
                    } catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException | InstantiationException | NoSuchMethodException e) {
                    }
                }
            }
        }
        return null;
    }
}

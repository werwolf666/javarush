package com.javarush.task.task31.task3106;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/*
Разархивируем файл
*/

public class Solution {
    public static void main(String[] args) {

        InputStream sequenceInputStream = null;
        List<String> filesList = new ArrayList<>(Arrays.asList(args));
        String resultName = filesList.remove(0);
        filesList.sort(Comparator.naturalOrder());
        try {
            for (int i = 0; i < filesList.size(); i++) {
//            for (int i = 1; i < args.length; i++) {
                    BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(filesList.get(i)));
                    if (sequenceInputStream == null) {
                        sequenceInputStream = inputStream;
                    } else {
                        sequenceInputStream = new SequenceInputStream(sequenceInputStream, inputStream);
                    }
            }
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(resultName));
            ZipInputStream zipInputStream = new ZipInputStream(sequenceInputStream);
            ZipEntry entry;
            byte[] buffer = new byte[2048];
            while ((entry = zipInputStream.getNextEntry()) != null) {
                int len;
                while ((len = zipInputStream.read(buffer)) > 0)
                {
                    bufferedOutputStream.write(buffer, 0, len);
                }
            }
            bufferedOutputStream.close();
            sequenceInputStream.close();
            zipInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }





    }
}


package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/* 
Что внутри папки?
*/
public class Solution {
    private static int dirCount = -1;
    private static int filesCount = 0;
    private static int size = 0;



    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String dir = reader.readLine();
        Path path = Paths.get(dir);
        if (dir.equals("") || !Files.isDirectory(path)) {
            System.out.println(dir + " - не папка");
        } else {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (Files.isDirectory(file)) {
                        Solution.dirCount++;
                    }
                    if (Files.isRegularFile(file)) {
                        Solution.filesCount++;
                        Solution.size += Files.size(file);
                    }

                    return super.visitFile(file, attrs);
                }

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    Solution.dirCount++;
                    Solution.size += Files.size(dir);
                    return super.preVisitDirectory(dir, attrs);

                }

            });

            System.out.println("Всего папок - " + dirCount);
            System.out.println("Всего файлов - " + filesCount);
            System.out.println("Общий размер - " + size);
        }
    }
}

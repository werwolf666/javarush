package com.javarush.task.task31.task3101;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
Проход по дереву файлов
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        File rootPath = new File(args[0]);
        File resultFileAbsolutePath = new File(args[1]);
        File newFile = new File(resultFileAbsolutePath.getParent() + File.separator + "allFilesContent.txt");
        FileUtils.renameFile(resultFileAbsolutePath, newFile);

        try (FileOutputStream fileOutputStream = new FileOutputStream(newFile)) {
            List<File> files = getFiles(rootPath);
            files.sort(new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
                }
            });
            for (File file :
                    files) {
                try (FileInputStream fileInputstream = new FileInputStream(file)) {
                    while (fileInputstream.available() > 0) {
                        fileOutputStream.write(fileInputstream.read());
                    }
/*
                    int left = 0;
                    do {
                        left = fileInputstream.available();
                        byte[] buffer = left < 8 * 1024 ? new byte[left] : new byte[8 * 1024];
                        fileInputstream.read(buffer, 0, buffer.length);
                        fileOutputStream.write(buffer);
                    } while (left > 0);
*/
                    fileOutputStream.write("\n".getBytes());
                }
            }
            fileOutputStream.flush();
        }


/*
        BufferedOutputStream fileOutputStream = new BufferedOutputStream(new FileOutputStream(newFile));
        for (File file :
                files) {
            BufferedInputStream fileInputstream = new BufferedInputStream(new FileInputStream(file));
            int left = 0;
            do {
                left = fileInputstream.available();
                byte[] buffer = left < 8 * 1024 ? new byte[left] : new byte[8 * 1024];
                fileInputstream.read(buffer, 0, buffer.length);
                fileOutputStream.write(buffer);
            } while (left > 0);
            fileInputstream.close();
            fileOutputStream.write("\n".getBytes());
        }
*/
    }

    public static List<File> getFiles(File rootPath) {
        List<File> list = new ArrayList<>();
        for (File file:
                rootPath.listFiles()) {
            if (file.isDirectory()) {
                list.addAll(getFiles(file));
            } else if (file.isFile()) {
                if (file.length() > 50) {
                    FileUtils.deleteFile(file);
                } else {
                    list.add(file);
                }
            }
        }
        return list;
    }


    public static void deleteFile(File file) {
        if (!file.delete()) System.out.println("Can not delete file with name " + file.getName());
    }
}

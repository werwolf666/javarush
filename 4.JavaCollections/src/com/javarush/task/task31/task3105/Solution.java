package com.javarush.task.task31.task3105;

import java.io.*;
import java.nio.Buffer;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/*
Добавление файла в архив
*/
public class Solution {
    public static void main(String[] args) throws IOException {

        String newFileName = args[0];
        String zipFileName = args[1];

        Map<String, byte[]> archiveFiles = new HashMap<>();

        File file = new File(newFileName);

        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFileName))) {
            ZipEntry zipEntry = zipInputStream.getNextEntry();
            while (zipEntry != null) {
                String fileName = zipEntry.getName();
                int size = (int) zipEntry.getSize();
                byte[] buffer = new byte[size];
                zipInputStream.read(buffer);
                archiveFiles.put(fileName, buffer);
                zipInputStream.closeEntry();
                zipEntry = zipInputStream.getNextEntry();
            }
        }

        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFileName))) {
            for (Map.Entry<String, byte[]> entry:
                 archiveFiles.entrySet()) {
                if (entry.getKey().substring(entry.getKey().lastIndexOf("/") + 1).equals(file.getName())) continue;
                ZipEntry zipEntry = new ZipEntry(entry.getKey());
                zipOutputStream.putNextEntry(zipEntry);
                zipOutputStream.write(entry.getValue());
                zipOutputStream.closeEntry();
            }

            ZipEntry zipEntry = new ZipEntry("new/" + file.getName());
            zipOutputStream.putNextEntry(zipEntry);
            Files.copy(file.toPath(), zipOutputStream);
            zipOutputStream.closeEntry();
        }
    }
}
package com.javarush.task.task28.task2810;

import com.javarush.task.task28.task2810.model.*;
import com.javarush.task.task28.task2810.view.HtmlView;

public class Aggregator {
    public static void main(String[] args) throws IllegalAccessException {
        Strategy strategy1 = new HHStrategy();
        Provider provider1 = new Provider(strategy1);
        Strategy strategy2 = new MoikrugStrategy();
        Provider provider2 = new Provider(strategy2);
        HtmlView view = new HtmlView();
        Model model = new Model(view, provider1, provider2);
        Controller controller = new Controller(model);
        view.setController(controller);
        view.userCitySelectEmulationMethod();
    }
}

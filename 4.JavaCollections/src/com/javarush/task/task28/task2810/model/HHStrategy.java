package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class HHStrategy implements Strategy {
    @Override
    public List<Vacancy> getVacancies(String searchString) {
        List<Vacancy> vacancyList = new ArrayList<>();
        Document document;
        try {
            int page = 0;
            while (true) {
                document = getDocument(searchString, page);
                Elements elements = document.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy");
                if (elements.isEmpty()) {
                    break;
                } else {
                    for (Element element :
                            elements) {
                        String url = element.select(".b-vacancy-list-link").attr("href");
                        String city = element.select(".searchresult__address").text();
                        String title = element.select(".b-vacancy-list-link").text();
                        String company = element.select("[data-qa=\"vacancy-serp__vacancy-employer\"]").text();
                        String siteName = "http://hh.ua";
                        String salary = element.select(".b-vacancy-list-salary").text();
                        Vacancy vacancy = new Vacancy();
                        vacancy.setUrl(url);
                        vacancy.setCity(city);
                        vacancy.setTitle(title);
                        vacancy.setCompanyName(company);
                        vacancy.setSiteName(siteName);
                        vacancy.setSalary(salary);
                        vacancyList.add(vacancy);
                    }
                    page++;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vacancyList;
    }

    private static final String URL_FORMAT = "http://hh.ua/search/vacancy?text=java+%s&page=%s";
//    private static final String URL_FORMAT = "http://javarush.ru/testdata/big28data.html";

    protected Document getDocument(String searchString, int page) throws IOException {
        return Jsoup.connect(String.format(URL_FORMAT, searchString, page)).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0").referrer("https://google.com.ua").get();
    }

}

package com.javarush.task.task28.task2810.view;

import com.javarush.task.task28.task2810.Controller;
import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.file.Path;
import java.util.List;

public class HtmlView implements View {
    private Controller controller;
    private final String filePath = "./4.JavaCollections/src/" + this.getClass().getPackage().getName().replaceAll("\\.", "/") + "/vacancies.html";

    @Override
    public void update(List<Vacancy> vacancies) {
        String s = getUpdatedFileContent(vacancies);
        updateFile(s);
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void userCitySelectEmulationMethod() {
        controller.onCitySelect("Odessa");
    }

    private String getUpdatedFileContent(List<Vacancy> vacancies) {
        try {
            Document document = getDocument();
            Element element = document.getElementsByClass("template").first();
            Element template = element.clone();
            template.removeAttr("style");
            template.removeClass("template");
            document.select("tr[class=vacancy]").not("tr[class=vacancy template").remove();
            for (Vacancy vacancy :
                    vacancies) {
                Element clone = template.clone();
                clone.getElementsByClass("city").first().text(vacancy.getCity());
                clone.getElementsByClass("companyName").first().text(vacancy.getCompanyName());
                clone.getElementsByClass("salary").first().text(vacancy.getSalary());
                clone.getElementsByTag("a").first().text(vacancy.getTitle());
                clone.getElementsByTag("a").first().attr("href", vacancy.getUrl());
                element.before(clone.outerHtml());
            }
            return document.outerHtml();
        } catch (IOException e) {
            e.printStackTrace();
            return "Some exception occurred";
        }
    }

    private void updateFile(String s) {
        try {
            FileWriter fileWriter = new FileWriter(new File(filePath));
            fileWriter.write(s);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected Document getDocument() throws IOException {
        return Jsoup.parse(new File(filePath), "UTF-8");
    }
}

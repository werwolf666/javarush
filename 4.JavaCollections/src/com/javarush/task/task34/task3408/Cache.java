package com.javarush.task.task34.task3408;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.WeakHashMap;

public class Cache<K, V> {
    private Map<K, V> cache = new WeakHashMap<K, V>();   //TODO add your code here

    public V getByKey(K key, Class<V> clazz) throws Exception {
        if (!cache.containsKey(key))
            cache.put(key, clazz.getConstructor(key.getClass()).newInstance(key));
        return cache.get(key);
    }

    public boolean put(V obj) {
        Method method = null;
        try {
            method = obj.getClass().getDeclaredMethod("getKey");
            method.setAccessible(true);
            Object[] args = new Object[0];
            try {
                K someKey = (K) method.invoke(obj, args);
                cache.put(someKey, obj);
            } catch (IllegalAccessException|InvocationTargetException e) {
                return false;
            }

        } catch (NoSuchMethodException e) {
            return false;
        }
        return true;
    }

    public int size() {
        return cache.size();
    }
}

package com.javarush.task.task26.task2613;

import com.javarush.task.task26.task2613.exception.InterruptOperationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ResourceBundle;

public class ConsoleHelper {
    private final static BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
    private static ResourceBundle res = ResourceBundle.getBundle(CashMachine.class.getPackage().getName() + ".resources.common_en");

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static String readString() throws InterruptOperationException {
        try {
            String s = bis.readLine();
            if (s.equalsIgnoreCase("exit")) {
                throw new InterruptOperationException();
            }
            return s;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String askCurrencyCode() throws InterruptOperationException {
        String code = null;
        writeMessage(res.getString("choose.currency.code"));
        while (true) {
            code = readString();
            if (code.length() == 3)
                break;
            else
                writeMessage(res.getString("invalid.data"));

        }
        return code.toUpperCase();
    }

    public static String[] getValidTwoDigits(String currencyCode) throws InterruptOperationException {
        writeMessage(res.getString("choose.denomination.and.count.format"));

        String[] input;
        while (true) {
            input = readString().split(" ");

            int nominal = 0;
            int total = 0;
            try {
                nominal = Integer.parseInt(input[0]);
                total = Integer.parseInt(input[1]);
            } catch (Exception e) {
                writeMessage(res.getString("invalid.data"));
                continue;
            }
            if (nominal <= 0 || total <= 0) {
                writeMessage(res.getString("invalid.data"));
                continue;
            }
            break;
        }
        return input;
    }

    public static Operation askOperation() throws InterruptOperationException {
        writeMessage(res.getString("choose.operation"));
        int code;

        while (true) {
            try {
                code = Integer.parseInt(readString());
                Operation operation = Operation.getAllowableOperationByOrdinal(code);
                return operation;
            } catch (InterruptOperationException e) {
                throw e;
            } catch (Exception e) {
                writeMessage(res.getString("invalid.data"));
                continue;
            }
        }
    }
    public static void printExitMessage() {
        writeMessage("Good Bye");
    }
}

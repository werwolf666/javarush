package com.javarush.task.task26.task2613;

public enum Operation {
    LOGIN, INFO, DEPOSIT, WITHDRAW, EXIT;

    public static Operation getAllowableOperationByOrdinal (Integer i) {
        Operation[] operations = values();
        if (i < 1 || i > 4) {
            throw new IllegalArgumentException();
        }
        return operations[i];
    }
}

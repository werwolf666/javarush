package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.CashMachine;
import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.CurrencyManipulator;
import com.javarush.task.task26.task2613.CurrencyManipulatorFactory;
import com.javarush.task.task26.task2613.exception.InterruptOperationException;
import com.javarush.task.task26.task2613.exception.NotEnoughMoneyException;

import java.util.ConcurrentModificationException;
import java.util.Map;
import java.util.ResourceBundle;

class WithdrawCommand implements Command
{
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "withdraw_en");
    @Override
    public void execute() throws InterruptOperationException
    {
        ConsoleHelper.writeMessage(res.getString("before"));
        String currencyCode = ConsoleHelper.askCurrencyCode();
        CurrencyManipulator cm = CurrencyManipulatorFactory.getManipulatorByCurrencyCode(currencyCode);

        boolean flag = true;
        while (flag) {
            ConsoleHelper.writeMessage(res.getString("specify.amount"));
            String temp = ConsoleHelper.readString();
            if (temp.matches("\\d+")) {
                int amount = Integer.valueOf(temp);
                if (cm.isAmountAvailable(amount)) {
                    Map<Integer, Integer> wa;
                    try
                    {
                        wa = cm.withdrawAmount(amount);
                        for (Map.Entry<Integer, Integer> pair : wa.entrySet()) {
                            System.out.println("\t" + pair.getKey() + " - " + pair.getValue());
                        }
                    }
                    catch (NotEnoughMoneyException e)
                    {
                        ConsoleHelper.writeMessage(res.getString("exact.amount.not.available"));
                    }
                    catch (ConcurrentModificationException e) {}
                    flag = false;
                } else {
                    ConsoleHelper.writeMessage(res.getString("not.enough.money"));
                }
            } else {
                ConsoleHelper.writeMessage(res.getString("specify.not.empty.amount"));
            }
        }
    }
}

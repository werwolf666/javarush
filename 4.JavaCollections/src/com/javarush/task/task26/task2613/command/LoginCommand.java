package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.CashMachine;
import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.exception.InterruptOperationException;

import java.util.Enumeration;
import java.util.Map;
import java.util.ResourceBundle;

public class LoginCommand implements Command {
    private final ResourceBundle validCreditCards = ResourceBundle.getBundle(CashMachine.class.getPackage().getName() + ".resources.verifiedCards");
    private ResourceBundle res = ResourceBundle.getBundle(CashMachine.RESOURCE_PATH + "login_en");
    @Override
    public void execute() throws InterruptOperationException {
        ConsoleHelper.writeMessage(res.getString("before"));

        Enumeration<String> cardNumbers = validCreditCards.getKeys();

        while (true) {
            ConsoleHelper.writeMessage(res.getString("specify.data"));
            String cardNumber = ConsoleHelper.readString();
            String pinCode = ConsoleHelper.readString();
            if (validCreditCards.containsKey(cardNumber)) {
                if (validCreditCards.getString(cardNumber).equals(pinCode)) {
                    boolean isLogin = false;
                    while (cardNumbers.hasMoreElements()) {
                        String c = cardNumbers.nextElement();
                        String p = validCreditCards.getString(c);
                        if (c.equals(cardNumber) && p.equals(pinCode)) {
                            ConsoleHelper.writeMessage(res.getString("success.format"));
                            isLogin = true;
                            break;
                        }
                    }
                    if (isLogin) {
                        break;
                    }
                } else {
                    ConsoleHelper.writeMessage(res.getString("not.verified.format"));
                }
            } else {
                ConsoleHelper.writeMessage(res.getString("try.again.with.details"));
                ConsoleHelper.writeMessage(res.getString("try.again.or.exit"));
            }
        }
    }
}

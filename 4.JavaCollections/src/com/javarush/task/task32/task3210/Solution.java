package com.javarush.task.task32.task3210;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

/* 
Используем RandomAccessFile
*/

public class Solution {
    public static void main(String... args) {
        String filename = args[0];
        int number = Integer.parseInt(args[1]);
        String text = args[2];
        try {
            RandomAccessFile file = new RandomAccessFile(filename, "rw");
            byte[] buffer = new byte[text.length()];
            file.seek(number);
            file.read(buffer, 0, text.length());
//            file.seek(file.length() < number ? file.length() : number);
            String result = Arrays.equals(buffer, text.getBytes()) ? "true" : "false";
            file.seek(file.length());
            file.write(result.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

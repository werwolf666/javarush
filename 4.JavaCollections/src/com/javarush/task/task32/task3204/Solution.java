package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;
import java.util.Random;

/* 
Генератор паролей
*/
public class Solution {
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        int password_length = 8;
        Random random = new Random();
        int big_start = 65;
        int big_count = 26;
        int small_start = 97;
        int small_count = 26;
        int figures_start = 48;
        int figures_count = 10;

        int random_big = random.nextInt(big_count) + big_start;
        int random_small = random.nextInt(small_count) + small_start;
        int random_figures = random.nextInt(figures_count) + figures_start;

        int position_big = random.nextInt(password_length);
        int position_small = random.nextInt(password_length);
        int position_figures = random.nextInt(password_length);

        int[] int_password = new int[password_length];
        int_password[position_big] = random_big;
        int_password[position_small] = random_small;
        int_password[position_figures] = random_figures;

        for (int i :
                int_password) {
            if (i == 0) {
                i = random.nextInt(big_count + small_count + figures_count);
                if (i > big_count + figures_count - 1) {
                    i = i - big_count - figures_count + small_start;
                } else if (i > figures_count - 1) {
                    i = i - figures_count + big_start;
                } else {
                    i = i + figures_start;
                }
            }
            stream.write(i);
        }

        return stream;
    }
}
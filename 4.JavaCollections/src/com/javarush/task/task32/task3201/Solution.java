package com.javarush.task.task32.task3201;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
Запись в существующий файл
*/
public class Solution {
    public static void main(String... args) {
        String filename = args[0];
        int number = Integer.parseInt(args[1]);
        String text = args[2];
        try {
            RandomAccessFile file = new RandomAccessFile(filename, "w");
            file.seek(file.length() < number ? file.length() : number);
            file.write(text.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;

/* 
Построй дерево(1)
*/
public class CustomTree extends AbstractList<String> implements Cloneable, Serializable{

    public Entry<String> root = new Entry<>("0");

    static class Entry<T> implements Serializable {
        String elementName;
        int lineNumber;
        boolean availableToAddLeftChildren, availableToAddRightChildren;
        Entry<T> parent, leftChild, rightChild;

        public Entry(String elementName) {
            this.elementName = elementName;
            this.availableToAddLeftChildren = true;
            this.availableToAddRightChildren = true;
        }

        void checkChildren() {
            if (leftChild != null) {
                this.availableToAddLeftChildren = false;
            }
            if (rightChild != null) {
                this.availableToAddRightChildren = false;
            }
        }

        boolean isAvailableToAddChildren() {
            return availableToAddLeftChildren || availableToAddRightChildren;
        }
    }

    public static void main(String[] args) {
        List<String> list = new CustomTree();
        for (int i = 1; i < 16; i++) {
            list.add(String.valueOf(i));
        }
        //System.out.println("Expected 3, actual is " + ((CustomTree) list).getParent("8"));
        list.remove("5");
        //System.out.println("Expected null, actual is " + ((CustomTree) list).getParent("11"));
    }

    public boolean add(String s) {
        Entry<String> newEntry = new Entry<>(s);
        if (this.root == null) {
            this.root = newEntry;
        } else {
            Entry<String> entry = this.root;
            entry = findBest(entry);
            entry.checkChildren();
            newEntry.parent = entry;
            newEntry.lineNumber = entry.lineNumber + 1;
            if (entry.availableToAddLeftChildren) {
                entry.leftChild = newEntry;
            } else {
                entry.rightChild = newEntry;
            }
        }
        return true;
    }

    Entry<String> findBest(Entry<String> entry) {
        entry.checkChildren();
        if (entry.isAvailableToAddChildren()) {
            return entry;
        } else {
            Entry<String> leftEntry = findBest(entry.leftChild);
            Entry<String> rightEntry = findBest(entry.rightChild);
            if (rightEntry.lineNumber < leftEntry.lineNumber) {
                return rightEntry;
            } else {
                return leftEntry;
            }
        }
    }

    Entry<String> findEntry(Entry<String> entry, Object o) {
        entry.checkChildren();
        Entry<String> foundEntry = null;
        if (entry.elementName.equals(o)) {
            foundEntry = entry;
        } else {
            if (entry.leftChild != null) {
                foundEntry = findEntry(entry.leftChild, o);
            }
            if (foundEntry == null && entry.rightChild != null) {
                foundEntry = findEntry(entry.rightChild, o);
            }
        }
        return foundEntry;
    }

    int childSize(Entry<String> entry) {
        entry.checkChildren();
        int leftSize = 0;
        int rightSize = 0;
        if (entry.leftChild != null) {
            leftSize = childSize(entry.leftChild);
        }
        if (entry.rightChild != null) {
            rightSize = childSize(entry.rightChild);
        }
        return leftSize + rightSize + 1;
    }

    public boolean remove(Object o) {
        Entry<String> entry = findEntry(this.root, o);
        if (entry == null) {
            return false;
        } else {
            if (entry.parent.rightChild == entry) {
                entry.parent.rightChild = null;
            } else {
                entry.parent.leftChild = null;
            }
            entry.parent = null;
            return true;
        }
    }

    public String getParent (String s) {
        Entry<String> entry = findEntry(this.root, s);
        return entry.parent.elementName;
    }

    @Override
    public void add(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String set(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return childSize(this.root) - 1;
    }
}

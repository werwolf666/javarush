package com.javarush.task.task13.task1319;

import java.io.*;
import java.util.ArrayList;

/* 
Запись в файл с консоли
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        String string;
        BufferedWriter outStream = new BufferedWriter(new FileWriter(filename));
        do {
            string = reader.readLine();
            outStream.write(string);
            outStream.newLine();
        } while (!string.equals("exit"));

        outStream.close();
        reader.close();
        // напишите тут ваш код
    }
}

package com.javarush.task.task13.task1318;

import java.io.*;
import java.util.Scanner;

/* 
Чтение файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream in = new FileInputStream(reader.readLine());

        while (in.available() > 0) {
            System.out.print((char)in.read());
        }
        System.out.println();
        in.close();
        reader.close();

/*

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        InputStream inStream = new FileInputStream(filename);
        OutputStream outStream = new BufferedOutputStream(System.out);
        while (inStream.available() > 0)
        {
            int data = inStream.read(); //читаем один байт из потока для чтения
            outStream.write(data); //записываем прочитанный байт в другой поток.
        }
        System.out.println();


        inStream.close();
        outStream.close();
        reader.close();

*/
        // напишите тут ваш код
    }
}
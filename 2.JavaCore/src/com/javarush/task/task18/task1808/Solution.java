package com.javarush.task.task18.task1808;

/* 
Разделение файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        String filename3 = reader.readLine();

        FileInputStream inputStream = new FileInputStream(filename1);
        FileOutputStream outputStream1 = new FileOutputStream(filename2);
        FileOutputStream outputStream2 = new FileOutputStream(filename3);

        if (inputStream.available() > 0) {
            //читаем весь файл одним куском
            byte[] buffer = new byte[inputStream.available()];
            int count = inputStream.read(buffer);
            int len2 = count / 2;
            int len1 = count - len2;
            outputStream1.write(buffer, 0, len1);
            outputStream2.write(buffer, len1, len2);
        }

        inputStream.close();
        outputStream1.close();
        outputStream2.close();

    }
}

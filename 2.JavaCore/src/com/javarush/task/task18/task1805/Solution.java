package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        TreeSet<Integer> set = new TreeSet();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        FileInputStream file = new FileInputStream(filename);
        int b;
        while (file.available() > 0) {
            b = file.read();
            set.add(b);
        }
        file.close();
        for (Integer i :
                set) {
            System.out.print(i + " ");
        }

    }
}

package com.javarush.task.task18.task1823;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String filename = reader.readLine();
            if (filename.equals("exit")) {
                break;
            }
            ReadThread thread = new ReadThread(filename);
            thread.start();

        }

    }

    public static class ReadThread extends Thread {
        private String fileName;
        public ReadThread(String fileName) throws FileNotFoundException {
            this.fileName = fileName;
            //implement constructor body
        }

        @Override
        public void run() {
            TreeMap<Integer, Integer> bytes = new TreeMap<Integer, Integer>();
            try {
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(this.fileName));
                while (bufferedInputStream.available() > 0) {
                    Integer i = bufferedInputStream.read();
                    if (bytes.containsKey(i)) {
                        int count = bytes.get(i);
                        bytes.replace(i, ++count);
                    } else {
                        bytes.put(i, 1);
                    }
                }
                bufferedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            int maxValue = 0;
            int maxKey = 0;
            for (Map.Entry<Integer, Integer> e:
                    bytes.entrySet()) {
                if (e.getValue() > maxValue) {
                    maxValue = e.getValue();
                    maxKey = e.getKey();
                }
            }
            resultMap.put(this.fileName, maxKey);
        }

        // implement file reading here - реализуйте чтение из файла тут
    }
}

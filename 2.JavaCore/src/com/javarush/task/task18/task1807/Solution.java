package com.javarush.task.task18.task1807;

/* 
Подсчет запятых
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        FileInputStream file = new FileInputStream(filename);
        int b;
        int count = 0;
        while (file.available() > 0) {
            b = file.read();
            if (b == ",".charAt(0)) {
                count++;
            }
        }
        file.close();
        System.out.println(count);
    }
}

package com.javarush.task.task18.task1822;

/* 
Поиск данных внутри файла
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        BufferedReader fileReader = new BufferedReader(new FileReader(filename));
        String s;
        while (fileReader.ready()) {
            s = fileReader.readLine();
            String[] array = s.split(" ");
            if (array[0].equals(args[0])) {
                System.out.println(s);
                break;
            }
        }
        fileReader.close();
    }
}

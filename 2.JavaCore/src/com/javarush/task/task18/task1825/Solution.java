package com.javarush.task.task18.task1825;

/* 
Собираем файл
*/

import javafx.collections.transformation.SortedList;

import java.io.*;
import java.util.TreeSet;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        TreeSet<String> sortedSet = new TreeSet<>();
        String outFilename = null;
        while (true) {
            String fileName = reader.readLine();
            if (fileName.equals("end")) {
                break;
            }
            sortedSet.add(fileName);
            if (outFilename == null) {
                outFilename = fileName.substring(0, fileName.lastIndexOf(".part"));
            }
        }
        BufferedWriter out = new BufferedWriter(new FileWriter(outFilename));
        for (String fileName:
             sortedSet) {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            while (in.ready()) {
                String s = in.readLine();
                out.write(s);
            }
            in.close();
        }
        out.close();

    }
}

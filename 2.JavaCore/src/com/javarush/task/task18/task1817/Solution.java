package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));

        int s;
        int sum = 0;
        int sum_pr = 0;
        double d =0;
        while (reader.ready()) {
            s = reader.read();
            if ((int) ' '  == s) {
                sum_pr++;
            }
            sum++;
        }
        d = (double) sum_pr / sum * 100;
        System.out.println(String.format("%.2f", d));
        reader.close();

    }
}

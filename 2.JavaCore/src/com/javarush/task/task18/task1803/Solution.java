package com.javarush.task.task18.task1803;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        FileInputStream file = new FileInputStream(filename);
        int b;
        int max = 0;
        Integer count;
        while (file.available() > 0) {
            b = file.read();
            count = map.get(b);
            if (count == null) {
                map.put(b, 1);
                if (max < 1) {
                    max = 1;
                }
            } else {
                count++;
                map.put(b, count);
                if (max < count) {
                    max = count;
                }
            }
        }
        file.close();
        for (Map.Entry<Integer, Integer> e :

                map.entrySet()) {
            if (e.getValue() == max) {
                System.out.print(e.getKey() + " ");
            }
        }
    }
}

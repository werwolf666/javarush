package com.javarush.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Минимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        FileInputStream file = new FileInputStream(filename);
        int min = Integer.MAX_VALUE;
        int b;
        while (file.available() > 0) {
            b = file.read();
            if (b < min) {
                min = b;
            }
        }
        System.out.println(min);
        file.close();
    }
}

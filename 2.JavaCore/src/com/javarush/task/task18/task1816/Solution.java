package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));

        int s;
        String pattern = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int sum = 0;
        while (reader.ready()) {
            s = reader.read();
            if (pattern.contains(Character.toString((char) s))) {
                sum++;
            }
/*
            if () {

            }
*/

        }
        System.out.println(sum);
        reader.close();

    }
}

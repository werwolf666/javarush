package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String filename1 = "d:\\1.txt";
//        String filename2 = "d:\\2.txt";
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();

        FileInputStream file1 = new FileInputStream(filename1);
        FileOutputStream file2 = new FileOutputStream(filename2);
        int s;
        StringBuilder sb = new StringBuilder();
        Integer i = 0;
        while (file1.available() > 0) {
            s = file1.read();
            if (s != (char) ' ') {
                sb.append((char) s);
            }
            if (s == (char) ' ' || file1.available() == 0) {
                i = Math.round(Float.parseFloat(sb.toString()));
                file2.write(i.toString().getBytes());
                sb.setLength(0);
                if (file1.available() > 0) {
                    file2.write((char) ' ');
                }
            }
        }
        file1.close();
        file2.close();

    }
}

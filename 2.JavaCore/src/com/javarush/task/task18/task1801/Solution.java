package com.javarush.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();

        FileInputStream file = new FileInputStream(filename);
        int max = 0;
        int b = 0;
        while (file.available() > 0) {
            b = file.read();
            if (b > max) {
                max = b;
            }
        }
        System.out.println(max);
        file.close();
    }
}

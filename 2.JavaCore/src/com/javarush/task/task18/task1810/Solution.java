package com.javarush.task.task18.task1810;

/* 
DownloadException
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws DownloadException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int size = 0;
        String filename;
        FileInputStream inputStream;
        do {
            filename = reader.readLine();
            inputStream = new FileInputStream(filename);
            size = inputStream.available();
            inputStream.close();
            if (size < 1000) {
                reader.close();
                throw new DownloadException();
            }
        } while (true);
    }

    public static class DownloadException extends Exception {

    }
}

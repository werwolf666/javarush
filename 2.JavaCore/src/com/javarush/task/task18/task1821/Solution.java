package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream file = new FileInputStream(args[0]);
        int i;
        char c;
        TreeMap<Character, Integer> symbols = new TreeMap<Character, Integer>();
        while (file.available() > 0) {
            i = file.read();
            c = (char) i;
            if (symbols.containsKey((char) i)) {
                int count = symbols.get(c);
                symbols.replace(c, ++count);
            } else {
                symbols.put(c, 1);
            }

        }
        file.close();
        for (Map.Entry<Character, Integer> e:
             symbols.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }

    }
}

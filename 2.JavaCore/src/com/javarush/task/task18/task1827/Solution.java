package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.oracle.jrockit.jfr.ContentType.Bytes;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufRead = new BufferedReader(new InputStreamReader(System.in));
        String fileOut = bufRead.readLine();
        bufRead.close();
        List<String> list = new ArrayList();
        BufferedReader readIn = new BufferedReader(new FileReader(fileOut));
        String str;
        final Pattern pattern = Pattern.compile("^\\d+");
        Matcher matcher;
        long maxNum = 0;
        while (readIn.ready()){
            str = readIn.readLine();
            list.add(str);
            matcher = pattern.matcher(str);
            long num = 0;
            while (matcher.find()) {
                num = Long.parseLong(matcher.group(0));
            }
            if (num > maxNum){
                maxNum = num;
            }
        }
        readIn.close();
        if (args[0].equals("-c")) {
            list.add(String.format("%-8d%-30s%-8s%-4s", (++maxNum), args[1], args[2], args[3]));
        }
        BufferedWriter bufWrite = new BufferedWriter(new FileWriter(fileOut));
        for (String s : list){
            bufWrite.write(s+"\n");
        }
        bufWrite.close();
        /*        if (args[0].equals("-c")) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String filename = reader.readLine();
//            String filename = "d:\\1.txt";

            BufferedReader fileReader = new BufferedReader(new FileReader(filename));
            long maxNum = 0;
            while (fileReader.ready()) {
                String s = fileReader.readLine();
                long num = Long.parseLong(s.substring(0,7).trim());
                if (maxNum < num) {
                    maxNum = num;
                }
            }
            fileReader.close();
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(filename, true));
            if (args[1].length() > 30) {
                args[1] = args[1].substring(0, 29);
            }
            if (args[2].length() > 8) {
                args[2] = args[2].substring(0, 7);
            }
            if (args[3].length() > 4) {
                args[3] = args[3].substring(0, 3);
            }
            fileWriter.write(String.format("%-8d%-30s%-8s%-4s", (++maxNum), args[1], args[2], args[3]) + "\n");
            fileWriter.close();
*/
/*

            RandomAccessFile file = new RandomAccessFile(filename, "rw");
            int size = (int) (file.length() / 80);
            int maxNum = 0;
            for (int i = 0; i < size; i++) {
                byte[] bytes = new byte[80];
                file.read(bytes);
                String s = new String(bytes);
                int num = Integer.parseInt(s.substring(0,7).trim());
                if (maxNum < num) {
                    maxNum = num;
                }
            }
            maxNum++;

            byte[] result = new byte[80];
            Arrays.fill(result, (byte) ' ');

            System.arraycopy(((Integer) maxNum).toString().getBytes(), 0, result, 0, ((Integer) maxNum).toString().getBytes().length);
            System.arraycopy(args[1].getBytes(), 0, result, 8, args[1].getBytes().length);
            System.arraycopy(args[2].getBytes(), 0, result, 68, args[2].getBytes().length);
            System.arraycopy(args[3].getBytes(), 0, result, 76, args[3].getBytes().length);
            file.write(result);

            file.close();

*/
        }
    //}
}

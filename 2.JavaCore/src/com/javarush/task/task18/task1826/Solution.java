package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileReader in = new FileReader(args[1]);
        FileWriter out = new FileWriter(args[2]);
        while (in.ready()) {
            int i = in.read();
            if (args[0].equals("-e")) {
                i--;
            } else if (args[0].equals("-d")) {
                i++;
            }
            out.write(i);
        }
        in.close();
        out.close();
    }

}

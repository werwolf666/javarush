package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        String filename3 = reader.readLine();
        FileOutputStream file1 = new FileOutputStream(filename1);
        FileInputStream file2 = new FileInputStream(filename2);
        FileInputStream file3 = new FileInputStream(filename3);
        int s;
        while (file2.available() > 0) {
            s = file2.read();
            file1.write(s);
        }
        file2.close();
        while (file3.available() > 0) {
            s = file3.read();
            file1.write(s);
        }
        file3.close();
        file1.close();
    }
}

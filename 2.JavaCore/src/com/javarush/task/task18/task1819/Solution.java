package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
//        String filename1 = "d:\\1.txt";
//        String filename2 = "d:\\2.txt";
        FileInputStream file1 = new FileInputStream(filename1);
        int s;
        ArrayList a = new ArrayList();
        StringBuilder sb = new StringBuilder();
        while (file1.available() > 0) {
            s = file1.read();
            sb.append((char) s);
        }
        file1.close();

        FileInputStream file2 = new FileInputStream(filename2);
        FileOutputStream file3 = new FileOutputStream(filename1);
        while (file2.available() > 0) {
            s = file2.read();
            file3.write(s);
        }
        file2.close();
        file3.write(sb.toString().getBytes());
        file3.close();
    }
}

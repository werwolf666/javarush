package com.javarush.task.task20.task2003;

import java.io.*;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Знакомство с properties
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        FileInputStream stream = new FileInputStream(filename);
        load(stream);
        FileOutputStream outstream = new FileOutputStream(filename);
        save(outstream);
        //implement this method - реализуйте этот метод
    }

    public void save(OutputStream outputStream) throws Exception {
        Properties prop = new Properties();
        prop.putAll(properties);
        prop.store(outputStream, "");
        PrintWriter writer = new PrintWriter(outputStream);
        //implement this method - реализуйте этот метод
    }

    public void load(InputStream inputStream) throws Exception {
        Properties prop = new Properties();
        prop.load(inputStream);
        properties.putAll((Map) prop);
        //implement this method - реализуйте этот метод
    }

    public static void main(String[] args) throws Exception {

        Solution sol = new Solution();
        sol.fillInPropertiesMap();

    }
}

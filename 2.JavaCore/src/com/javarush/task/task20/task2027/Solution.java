package com.javarush.task.task20.task2027;

import java.util.ArrayList;
import java.util.List;

/* 
Кроссворд
*/
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };
        detectAllWords(crossword, "jr", "home", "same", "poe", "rr", "oe", "mm", "vo", "jrgs", "vorg", "aaa");
        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        List<Word> result = new ArrayList<>();
        for (String word_s :
                words) {
            Word word = new Word(word_s);
            int currentCharNumber = 0;
            boolean wordFound = false;
            char nextChar = word.text.charAt(currentCharNumber);
            for (int i = 0; i < crossword.length; i++) {
                for (int j = 0; j < crossword[i].length; j++) {
                    if (nextChar == crossword[i][j]) {
                        word.setStartPoint(j, i);
                        currentCharNumber++;
                        nextChar = word.text.charAt(currentCharNumber);
                        for (int k = i - 1; k <= i + 1; k++) {
                            if (k < 0 || k > crossword.length - 1) {
                                continue;
                            }
                            for (int l = j - 1; l <= j + 1 ; l++) {
                                if (l < 0 || (i == k && j == l) || l > crossword[k].length - 1) {
                                    continue;
                                }
                                if (nextChar == crossword[k][l]) {
                                    int stepX = k - i;
                                    int stepY = l - j;
                                    int m = k;
                                    int n = l;

                                    if (word.text.length() == 2) {
                                        word.setEndPoint(n, m);
                                        wordFound = true;
                                        result.add(word);
                                    } else {

                                        while (!(m < 0 || m > crossword.length - 1 || n < 0 || n > crossword[m].length - 1) && currentCharNumber <= word.text.length()) {
                                            currentCharNumber++;
                                            nextChar = word.text.charAt(currentCharNumber);
                                            m += stepX;
                                            n += stepY;
                                            if (m < 0 || m > crossword.length - 1 || n < 0 || n > crossword[m].length - 1 || nextChar != crossword[m][n]) {
                                                break;
                                            }
                                            if (currentCharNumber == word.text.length() - 1) {
                                                word.setEndPoint(n, m);
                                                wordFound = true;
                                                result.add(word);
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (wordFound) {
                                    break;
                                } else {
                                    currentCharNumber = 1;
                                    nextChar = word.text.charAt(currentCharNumber);
                                }
                            }
                            if (wordFound) {
                                break;
                            } else {
                                currentCharNumber = 0;
                                nextChar = word.text.charAt(currentCharNumber);
                            }
                        }
                    }
                    if (wordFound) {
                        break;
                    }
                }
                if (wordFound) {
                    break;
                }
            }
        }
        return result;
    }

    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}

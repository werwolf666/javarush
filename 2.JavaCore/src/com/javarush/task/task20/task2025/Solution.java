package com.javarush.task.task20.task2025;

import java.util.ArrayList;
import java.util.List;

/*
Алгоритмы-числа
*/
public class Solution {
    public static long[] getNumbers(long N) {
        List list = ArmstrongNumbersMultiSetLongOpt.generate((int) (Math.log10(N)));
        long[] result = new long[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = (long) list.get(i);
        }

        return result;
/*
        long[] result = null;
        ArrayList<Long> arr = new ArrayList<Long>();
        String nS = Long.toString(N);
        int razr = nS.length();
        long[][] pows = new long[10][razr + 1];
        for (int i = 0; i < pows.length; i++) {
            long p = 1;
            for (int j = 0; j < pows[i].length; j++) {
                pows[i][j] = p;
                p *= i;
            }
        }

        for (Long i = (long) 370; i < Math.pow(10, razr); i++) {
//            System.out.println(i);
            Integer m = (int) Math.ceil(Math.log10(i));

            long ost = i;
            long sum = 0;
            for (int j = 0; j < m; j++) {
                int cifra = (int) (ost % 10);
//                sum += Math.pow(Integer.parseInt(s.substring(j, j + 1)), m);
                sum += pows[cifra][m];
                ost = (ost - cifra) / 10;
            }
            if (sum == i) {
                arr.add(sum);
            }
        }
        result = new long[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            result[i] = arr.get(i);
        }


        return result;
*/    }

    public static void main(String[] args) {
        long[] res = new long[500];
        res = getNumbers(3711);
//        res = getNumbers(Long.MAX_VALUE);

    }
}

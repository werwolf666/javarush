package com.javarush.task.task20.task2026;

/* 
Алгоритмы-прямоугольники
*/
public class Solution {
    public static void main(String[] args) {
        byte[][] a = new byte[][]{


                {1, 1, 0, 0, 1, 0},
                {1, 1, 0, 0, 1, 0},
                {1, 1, 0, 0, 0, 0},
                {1, 1, 0, 1, 1, 0},

/*



                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 1}
*/
        };
        int count = getRectangleCount(a);
        System.out.println("count = " + count + ". Должно быть 2");

    }

    public static int getRectangleCount(byte[][] a) {
        boolean isRect = false;
        int xMin = 0;
        int yMin = 0;
        int xMax = 0;
        int yMax = 0;
        int result = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (isRect ) {
                    if (a[i][j] == 0) {
                        j--;
                        xMax = j;
                        do {
                            i++;
                            if (i == a.length || j == a[i].length || a[i][j] == 0) {
                                i--;
                                yMax = i;
                                break;
                            }
                        } while (i < a.length - 1);
                        yMax = i;
                        isRect = false;
                        for (int k = yMin; k <= yMax ; k++) {
                            for (int l = xMin; l <= xMax; l++) {
                                a[k][l] = 2;
                            }
                        }
                        i = 0;
                        j = 0;
                    }
                } else {
                    if (a[i][j] == 1) {
                        isRect = true;
                        result++;
                        xMin = j;
                        yMin = i;
                    }
                }
            }
        }
        return result;
    }
}

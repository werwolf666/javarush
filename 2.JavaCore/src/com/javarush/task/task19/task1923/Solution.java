package com.javarush.task.task19.task1923;

/* 
Слова с цифрами
*/

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(args[1]));
        StringBuilder sb = new StringBuilder();
        while (fileReader.ready()) {
            String s = fileReader.readLine();

            Pattern p = Pattern.compile("\\S*\\d+\\S*");
            Matcher m = p.matcher(s);
            while (m.find()) {
                sb.append(m.group() + " ");
            }
        }
        fileWriter.write(sb.toString().trim());
        fileReader.close();
        fileWriter.close();

    }
}

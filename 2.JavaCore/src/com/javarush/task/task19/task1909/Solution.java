package com.javarush.task.task19.task1909;

/* 
Замена знаков
*/

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String inFilename = "d:\\1.txt";
//        String outFilename = "d:\\2.txt";
        String inFilename = reader.readLine();
        String outFilename = reader.readLine();
        BufferedReader in = new BufferedReader(new FileReader(inFilename));
        BufferedWriter out = new BufferedWriter(new FileWriter(outFilename));
        while (in.ready()) {
            String s = in.readLine();
            out.write(s.replace(".", "!"));
        }
        reader.close();
        in.close();
        out.close();

    }
}

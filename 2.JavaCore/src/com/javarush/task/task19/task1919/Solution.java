package com.javarush.task.task19.task1919;

/* 
Считаем зарплаты
*/

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        TreeMap<String , Double> map = new TreeMap<>();
        while (fileReader.ready()) {
            String s = fileReader.readLine();
            String[] arr = s.split(" ");
            if (map.containsKey(arr[0])) {
                map.replace(arr[0], map.get(arr[0]) + Double.parseDouble(arr[1]));
            } else {
                map.put(arr[0], Double.parseDouble(arr[1]));
            }
        }
        fileReader.close();

        for (Map.Entry<String, Double> e:
             map.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }
}

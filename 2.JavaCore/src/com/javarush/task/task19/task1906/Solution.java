package com.javarush.task.task19.task1906;

/* 
Четные байты
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inFilename = reader.readLine();
        String outFilename = reader.readLine();
        FileReader in = new FileReader(inFilename);
        FileWriter out = new FileWriter(outFilename);
        boolean is_even = false;
        while (in.ready()) {
            int i = in.read();
            if (is_even) {
                out.write(i);
            }
            is_even = !is_even;
        }
        reader.close();
        in.close();
        out.close();

    }
}

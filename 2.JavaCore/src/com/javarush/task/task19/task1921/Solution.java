package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        while (fileReader.ready()) {
            double d = 0;
            String s = fileReader.readLine();
            String[] arr = s.split(" ");
            StringBuilder name = new StringBuilder();
            for (int i = 0; i < arr.length - 3; i++) {

                name.append(arr[i] + " ");
            }
            PEOPLE.add(new Person(name.toString().trim(), new Date(arr[arr.length-2] + "/" + arr[arr.length-3] + "/" + arr[arr.length-1])));
        }
        fileReader.close();

    }
}

package com.javarush.task.task19.task1914;

/* 
Решаем пример
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outputStream);
        System.setOut(stream);
        testString.printSomething();
        String s = outputStream.toString();
        String[] s_a = s.split(" ");
        int result = 0;
        if (s_a[1].equals("+") ) {
            result = Integer.parseInt(s_a[0]) + Integer.parseInt(s_a[2]);
        } else if (s_a[1].equals("-") ) {
            result = Integer.parseInt(s_a[0]) - Integer.parseInt(s_a[2]);
        } else if (s_a[1].equals("*") ) {
            result = Integer.parseInt(s_a[0]) * Integer.parseInt(s_a[2]);
        }
        System.setOut(consoleStream);
        System.out.println(s.trim() + " " + String.valueOf(result));
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}


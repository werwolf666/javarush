package com.javarush.task.task19.task1920;

/* 
Самый богатый
*/

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        TreeMap<String , Double> map = new TreeMap<>();
        double max = 0.;
        while (fileReader.ready()) {
            double d = 0;
            String s = fileReader.readLine();
            String[] arr = s.split(" ");
            if (map.containsKey(arr[0])) {
                d = map.get(arr[0]) + Double.parseDouble(arr[1]);
                map.replace(arr[0], d);
            } else {
                d = Double.parseDouble(arr[1]);
                map.put(arr[0], d);
            }
            if (d > max) {
                max = d;
            }
        }
        fileReader.close();

        for (Map.Entry<String, Double> e:
                map.entrySet()) {
            if (e.getValue() == max) {
                System.out.println(e.getKey());
            }
        }
    }
}

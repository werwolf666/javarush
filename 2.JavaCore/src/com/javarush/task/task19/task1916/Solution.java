package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
//        String fileName1 = "d:\\1.txt";
//        String fileName2 = "d:\\2.txt";

        reader.close();

        BufferedReader fileReader1 = new BufferedReader(new FileReader(fileName1));
        BufferedReader fileReader2 = new BufferedReader(new FileReader(fileName2));
        String tmp1 = null;
        String tmp2 = null;
        while (fileReader1.ready()) {
            String s1 = fileReader1.readLine();
            if (s1.equals(tmp1)) {
                lines.add(new LineItem(Type.SAME, s1));
                tmp1 = tmp2;
                tmp2 = null;
            } else {
                if (tmp1 != null) {
                    if (tmp2 == null) {
                        tmp2 = fileReader2.readLine();
                        if (s1.equals(tmp2)) {
                            lines.add(new LineItem(Type.ADDED, tmp1));
                            tmp1 = tmp2;
                            tmp2 = null;
                            if (s1.equals(tmp1)) {
                                lines.add(new LineItem(Type.SAME, s1));
                                tmp1 = null;
                            }
                        } else {
                            lines.add(new LineItem(Type.REMOVED, s1));
                        }
                    }
                } else {
                    if (fileReader2.ready()) {
                        String s2 = fileReader2.readLine();
                        if (s1.equals(s2)) {
                            lines.add(new LineItem(Type.SAME, s1));
                        } else {
                            String s3 = fileReader2.readLine();
                            if (s1.equals(s3)) {
                                lines.add(new LineItem(Type.ADDED, s2));
                                lines.add(new LineItem(Type.SAME, s1));
                                tmp1 = null;
                                tmp2 = null;
                            } else {
                                lines.add(new LineItem(Type.REMOVED, s1));
                                tmp1 = s2;
                                tmp2 = s3;
                            }
                        }

                    } else {
                        lines.add(new LineItem(Type.REMOVED, s1));
                    }
                }
            }
        }
        fileReader1.close();
        if (tmp1 != null) {
            lines.add(new LineItem(Type.ADDED, tmp1));
        }
        if (tmp2 != null) {
            lines.add(new LineItem(Type.ADDED, tmp2));
        }
        while (fileReader2.ready()) {
            String s2 = fileReader2.readLine();
            lines.add(new LineItem(Type.ADDED, s2));
        }
        fileReader2.close();
;
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}

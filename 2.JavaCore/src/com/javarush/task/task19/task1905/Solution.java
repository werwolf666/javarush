package com.javarush.task.task19.task1905;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Закрепляем адаптер
*/

public class Solution {
    public static Map<String,String> countries = new HashMap<String,String>();

    static {
        countries.put("UA", "Ukraine");
        countries.put("RU", "Russia");
        countries.put("CA", "Canada");
    }


    public static void main(String[] args) {
//        System.out.println("callto://" + "+38(050)123-45-67".replaceAll("[\\(\\)-]", ""));
//            if (m.matches()) {
//                return "callto://" + m.group(1) + m.group(2) + m.group(3) + m.group(4) + m.group(5);
//            } else {
//                return null;
//            }
    }

    public static class DataAdapter implements RowItem {
        private Customer customer;
        private Contact contact;

        public DataAdapter(Customer customer, Contact contact) {
            this.contact = contact;
            this.customer = customer;
        }

        @Override
        public String getCountryCode() {
            for (Map.Entry<String, String> e :
                    countries.entrySet()) {
                if (e.getValue().equals(customer.getCountryName())) {
                    return e.getKey();
                }
            }
            return null;
        }

        @Override
        public String getCompany() {
            return customer.getCompanyName();
        }

        @Override
        public String getContactFirstName() {
            return contact.getName().split(", ")[1];
        }

        @Override
        public String getContactLastName() {
            return contact.getName().split(", ")[0];
        }

        @Override
        public String getDialString() {
//            Pattern p = Pattern.compile("(\\+\\d{2})\\((\\d{3})\\)(\\d{3})-(\\d{2})-(\\d{2})");
//            Matcher m = p.matcher(contact.getPhoneNumber());
            return "callto://" + contact.getPhoneNumber().replaceAll("[\\(\\)-]", "");
//            if (m.matches()) {
//                return "callto://" + m.group(1) + m.group(2) + m.group(3) + m.group(4) + m.group(5);
//            } else {
//                return null;
//            }
        }
    }

    public static interface RowItem {
        String getCountryCode();        //example UA
        String getCompany();            //example JavaRush Ltd.
        String getContactFirstName();   //example Ivan
        String getContactLastName();    //example Ivanov
        String getDialString();         //example callto://+380501234567
    }

    public static interface Customer {
        String getCompanyName();        //example JavaRush Ltd.
        String getCountryName();        //example Ukraine
    }

    public static interface Contact {
        String getName();               //example Ivanov, Ivan
        String getPhoneNumber();        //example +38(050)123-45-67
    }
}
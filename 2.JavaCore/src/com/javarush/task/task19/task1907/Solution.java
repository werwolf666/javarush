package com.javarush.task.task19.task1907;

/* 
Считаем слово
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
//        String filename = "d:\\1.txt";
        BufferedReader in = new BufferedReader(new FileReader(filename));
        char[] buf;
        Pattern p = Pattern.compile("\\bworld\\b");
        int count = 0;
        while (in.ready()) {
            String s = in.readLine();
            Matcher m = p.matcher(s);
            while(m.find()) count++;
        }
        System.out.println(count);
        reader.close();
        in.close();
    }
}

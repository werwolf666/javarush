package com.javarush.task.task19.task1918;

/* 
Знакомство с тегами
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String filename = "d:\\1.txt";
        String filename = reader.readLine();
        reader.close();
        String tag = args[0];

        BufferedReader fileReader = new BufferedReader(new FileReader(filename));
        StringBuilder sb = new StringBuilder();
        while (fileReader.ready()) {
            sb.append(fileReader.readLine());
        }
        fileReader.close();

        int index = 0;
        ArrayList<Integer> tagBegin = new ArrayList<>();
        ArrayList<Integer> tagEnd = new ArrayList<>();
        while (index < sb.length()) {
            int i = sb.indexOf("<" + tag, index);
            if (i == -1) {
                index = sb.length();
            } else {
                tagBegin.add(i);
                index = i + 1;
            }
        }

        index = 0;
        while (index < sb.length()) {
            int i = sb.indexOf("</" + tag, index);
            if (i == -1) {
                index = sb.length();
            } else {
                i = sb.indexOf(">", i);
                tagEnd.add(i);
                index = i + 1;
            }
        }
        TreeMap<Integer, String> allTags = new TreeMap<>();
        for (Integer i :
             tagBegin) {
            allTags.put(i, "Begin");
        }
        for (Integer i :
                tagEnd) {
            allTags.put(i, "End");
        }

        while (!allTags.isEmpty()) {
            int beginCount = 0;
            int endCount = 0;
            int indexBegin = 0;
            for (Map.Entry<Integer, String> e:
                    allTags.entrySet()) {
                if (e.getValue().equals("Begin")) {
                    if (beginCount == 0) {
                        indexBegin = e.getKey();
                    }
                    beginCount++;
                }
                if (e.getValue().equals("End")) {
                    endCount++;
                }
                if (beginCount == endCount) {
                    System.out.println(sb.substring(indexBegin, e.getKey() + 1));
                    allTags.remove(indexBegin);
                    allTags.remove(e.getKey());
                    break;
                }
            }
        }

    }
}

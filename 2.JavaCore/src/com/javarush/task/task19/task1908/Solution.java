package com.javarush.task.task19.task1908;

/* 
Выделяем числа
*/

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String inFilename = "d:\\1.txt";
//        String outFilename = "d:\\2.txt";
        String inFilename = reader.readLine();
        String outFilename = reader.readLine();
        BufferedReader in = new BufferedReader(new FileReader(inFilename));
        BufferedWriter out = new BufferedWriter(new FileWriter(outFilename));
        Pattern p = Pattern.compile("\\b\\d+\\b");
        while (in.ready()) {
            String s = in.readLine();
            Matcher m = p.matcher(s);
            ArrayList<String> arrayList = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            while(m.find())  {
                sb.append(m.group() + " ");
            }
            sb.deleteCharAt(sb.length()-1);
            out.write(sb.toString());
        }
        reader.close();
        in.close();
        out.close();

    }
}

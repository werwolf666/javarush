package com.javarush.task.task19.task1903;

/*
Адаптация нескольких интерфейсов
*/

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static Map<String, String> countries = new HashMap<String, String>();

    static {
        countries.put("UA", "Ukraine");
        countries.put("RU", "Russia");
        countries.put("CA", "Canada");
    }

    public static void main(String[] args) {
    }

    public static class IncomeDataAdapter implements IncomeData{
        private Customer customer;
        private Contact contact;

        public IncomeDataAdapter(Customer customer, Contact contact) {
            this.customer = customer;
            this.contact = contact;
        }

        @Override
        public String getCountryCode() {
            for (Map.Entry<String, String> e :
                    countries.entrySet()) {
                if (e.getValue().equals(customer.getCountryName())) {
                    return e.getKey();
                }
            }
            return null;
        }

        @Override
        public String getCompany() {
            return customer.getCompanyName();
        }

        @Override
        public String getContactFirstName() {
            String[] fulName = contact.getName().split(",");
            return fulName[1].trim();
        }

        @Override
        public String getContactLastName() {
            String[] fulName = contact.getName().split(",");
            return fulName[0].trim();
        }

        @Override
        public int getCountryPhoneCode() {
            Pattern p = Pattern.compile("\\+(\\d{2})\\((\\d{3})\\)(\\d{3})-(\\d{2})-(\\d{2})");
            Matcher m = p.matcher("+38(050)123-45-67");
            if (m.matches()) {
                return Integer.parseInt(String.format("%10s", m.group(2) + m.group(3) + m.group(4) + m.group(5)).replace(" ", "0"));
            } else {
                return 0;
            }
        }

        @Override
        public int getPhoneNumber() {
            Pattern p = Pattern.compile("\\+(\\d{2})\\((\\d{3})\\)(\\d{3})-(\\d{2})-(\\d{2})");
            Matcher m = p.matcher("+38(050)123-45-67");
            if (m.matches()) {
                return Integer.parseInt(m.group(1));
            } else {
                return 0;
            }
        }
    }


    public static interface IncomeData {
        String getCountryCode();        //example UA

        String getCompany();            //example JavaRush Ltd.

        String getContactFirstName();   //example Ivan

        String getContactLastName();    //example Ivanov

        int getCountryPhoneCode();      //example 38

        int getPhoneNumber();           //example 501234567
    }

    public static interface Customer {
        String getCompanyName();        //example JavaRush Ltd.

        String getCountryName();        //example Ukraine
    }

    public static interface Contact {
        String getName();               //example Ivanov, Ivan

        String getPhoneNumber();        //example +38(050)123-45-67
    }
}
package com.javarush.task.task19.task1925;

/* 
Длинные слова
*/

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(args[1]));
        StringBuilder sb = new StringBuilder();
        while (fileReader.ready()) {
            String[] ar = fileReader.readLine().split(" ");
            for (String s :
                    ar) {
                if (s.length() > 6) {
                    sb.append(s + ",");
                }
            }

        }
        sb.setLength(sb.length() - 1);
        fileWriter.write(sb.toString());
        fileReader.close();
        fileWriter.close();

    }
}

package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        int count = 0;
        switch (args[0]) {
            case "-c":
                synchronized (allPeople) {
                    count = (args.length - 1) / 3;
                    for (int i = 0; i < count; i++) {
                        if (args[(i * 3) + 2].equals("м")) {
                            allPeople.add(Person.createMale(args[(i * 3) + 1], sdf.parse(args[(i * 3) + 3])));
                            System.out.println(allPeople.size() - 1);
                        } else if (args[(i * 3) + 2].equals("ж")) {
                            allPeople.add(Person.createFemale(args[(i * 3) + 1],  sdf.parse(args[(i * 3) + 3])));
                            System.out.println(allPeople.size() - 1);
                        }
                    }
                }
                break;
            case "-u":
                synchronized (allPeople) {
                    count = (args.length - 1) / 4;
                    for (int i = 0; i < count; i++) {
                        Person person = allPeople.get(Integer.parseInt(args[(i * 4) + 1]));
                        person.setName(args[(i * 4) + 2]);
                        if (args[(i * 4) + 3].equals("м")) {
                            person.setSex(Sex.MALE);
                        } else if (args[(i * 4) + 3] == "ж") {
                            person.setSex(Sex.FEMALE);
                        }
                        person.setBirthDay(sdf.parse(args[(i * 4) + 4]));
                    }
                }
                break;
            case "-d":
                synchronized (allPeople) {
                    count = args.length - 1;
                    for (int i = 0; i < count; i++) {
                        Person person = allPeople.get(Integer.parseInt(args[i + 1]));
                        person.setName(null);
                        person.setSex(null);
                        person.setBirthDay(null);
                    }
                }
                break;
            case "-i":
                synchronized (allPeople) {
                    count = args.length - 1;
                    String sex = "";
                    for (int i = 0; i < count; i++) {
                        Person person = allPeople.get(Integer.parseInt(args[i + 1]));
                        if (person.getSex() == Sex.MALE) {
                            sex = "м";
                        } else if (person.getSex() == Sex.FEMALE) {
                            sex = "ж";
                        }
                        System.out.println(person.getName() + " " + sex + " " + sdf1.format(person.getBirthDay()));
                    }
                }
                break;

        }
        System.out.println("");
        //start here - начни тут
    }
}

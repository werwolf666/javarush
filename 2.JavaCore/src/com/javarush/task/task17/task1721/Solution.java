package com.javarush.task.task17.task1721;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        BufferedReader fileReader = new BufferedReader(new FileReader(filename1));

        String s = "";
        do {
            s = fileReader.readLine();
            if (s == null) {
                break;
            }
            allLines.add(s);
        } while (true);
        fileReader.close();
        fileReader = new BufferedReader(new FileReader(filename2));
        do {
            s = fileReader.readLine();
            if (s == null) {
                break;
            }
            forRemoveLines.add(s);
        } while (true);
        fileReader.close();
        Solution solution = new Solution();
        solution.joinData();
    }

    public void joinData () throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)) {
            allLines.removeAll(forRemoveLines);
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}

package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date("15/04/1990")));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        Person person = null;
        switch (args[0]) {
            case "-c":
                if (args[2].equals("м")) {
                    allPeople.add(Person.createMale(args[1], sdf.parse(args[3])));
                    System.out.println(allPeople.size() - 1);
                } else if (args[2] == "ж") {
                    allPeople.add(Person.createFemale(args[1],  sdf.parse(args[3])));
                    System.out.println(allPeople.size() - 1);
                }
                break;
            case "-u":
                person = allPeople.get(Integer.parseInt(args[1]));
                person.setName(args[2]);
                if (args[3].equals("м")) {
                    person.setSex(Sex.MALE);
                } else if (args[3] == "ж") {
                    person.setSex(Sex.FEMALE);
                }
                person.setBirthDay(sdf.parse(args[4]));
                break;
            case "-d":
                person = allPeople.get(Integer.parseInt(args[1]));
                person.setName(null);
                person.setSex(null);
                person.setBirthDay(null);
                break;
            case "-i":
                String sex = "";
                person = allPeople.get(Integer.parseInt(args[1]));
                if (person.getSex() == Sex.MALE) {
                    sex = "м";
                } else if (person.getSex() == Sex.FEMALE) {
                    sex = "ж";
                }
                System.out.println(person.getName() + " " + sex + " " + sdf1.format(person.getBirthDay()));
                break;
            default:
                break;

        }
        //start here - начни тут
    }
}

package com.javarush.task.task12.task1217;

/* 
Интерфейсы Fly, Run, Swim
*/

public class Solution {
    public static void main(String[] args) {

    }

    public interface Fly {
        int quickFly();
    }
    public interface Run {
        int quickRun();
    }
    public interface Swim {
        int quickSwim();
    }
//add interfaces here - добавь интерфейсы тут

}

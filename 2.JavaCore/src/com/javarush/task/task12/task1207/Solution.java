package com.javarush.task.task12.task1207;

/* 
print(int) и print(Integer)
*/

public class Solution {
    public static void main(String[] args) {
        int i = 1;
        print(i);
        print((Integer) i);


    }

    public static void print(int i) {
        System.out.println("int" + i);
    }
    public static void print(Integer i) {
        System.out.println("Integer" + i);
    }
    //Напишите тут ваши методы
}

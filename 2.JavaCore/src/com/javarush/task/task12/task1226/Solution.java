package com.javarush.task.task12.task1226;

/* 
Fly, Run, Climb для классов Cat, Dog, Tiger, Duck
*/

public class Solution {

    interface Fly {
        void fly();
    }

    interface Climb {
        void climb();
    }

    interface Run {
        void run();
    }

    public class Cat implements Climb, Run{
        public void climb() {

        }
        public void run() {

        }
    }

    public class Dog implements Run{
        public void run() {

        }
    }

    public class Tiger extends Cat {
    }

    public class Duck implements Fly, Run{
        public void fly() {

        }
        public void run() {

        }
    }
}

package com.javarush.task.task14.task1409;

/**
 * Created by alex.vol on 07.04.2017.
 */
public class SuspensionBridge implements Bridge {
    @Override
    public int getCarsCount() {
        return 1;
    }
}

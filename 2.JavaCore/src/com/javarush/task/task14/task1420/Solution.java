package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = "";
        int i1 = 0;
        int i2 = 0;
        s = reader.readLine();
        i1 = Integer.parseInt(s);
        s = reader.readLine();
        i2 = Integer.parseInt(s);
        if (i1 <= 0 || i2 <=0) {
            throw new Exception();
        }
        int min;
        int max;
        if (i1 < i2) {
            min = i1;
            max = i2;
        } else {
            min = i2;
            max = i1;
        }
        for (int i = max; i > 0; i--) {
            if (max % i == 0 && min % i == 0) {
                System.out.println(i);
                break;
            }
        }
    }
}

package com.javarush.task.task14.task1413;

/**
 * Created by wolf on 10.04.2017.
 */
public class Monitor implements CompItem {

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
}

package com.javarush.task.task14.task1408;

/**
 * Created by alex.vol on 05.04.2017.
 */
public abstract class Hen {
    abstract int getCountOfEggsPerMonth();
    String getDescription() {
        return "Я - курица.";
    }
}

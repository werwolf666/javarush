package com.javarush.task.task14.task1408;

/**
 * Created by alex.vol on 05.04.2017.
 */
public class RussianHen extends Hen {
    @Override
    String getDescription() {
        return super.getDescription() + " Моя страна - " + Country.RUSSIA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
    }

    @Override
    int getCountOfEggsPerMonth() {
        return 2;
    }
}

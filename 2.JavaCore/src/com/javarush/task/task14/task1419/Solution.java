package com.javarush.task.task14.task1419;

import java.util.ArrayList;
import java.util.List;

/* 
Нашествие исключений
*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   //it's first exception
        try {
            float i = 1 / 0;
        } catch (ArithmeticException e) {
            exceptions.add(e);
        }
        try {
            String s = "FOOBAR";
            int i = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            exceptions.add(e);
        }
        try {
            int array[] = {20,20,40};
            int j = array[3];
        } catch (ArrayIndexOutOfBoundsException e) {
            exceptions.add(e);
        }
        try {
            String str="easysteps2buildwebsite";
            System.out.println(str.length());
            char c = str.charAt(0);
            c = str.charAt(40);
        } catch (StringIndexOutOfBoundsException e) {
            exceptions.add(e);
        }
        try {
            String str=null;
            System.out.println (str.length());
        } catch (NullPointerException e) {
            exceptions.add(e);
        }
        try {
            Object x[] = new String[3];
            x[0] = new Integer(0);
        } catch (ArrayStoreException e) {
            exceptions.add(e);
        }
        try {
            Object x = new Integer(0);
            System.out.println((String)x);
        } catch (ClassCastException e) {
            exceptions.add(e);
        }
        try {
            throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            exceptions.add(e);
        }
        try {
            int[] array = new int[-3];
        } catch (NegativeArraySizeException e) {
            exceptions.add(e);
        }
        try {
            float i = 1 / 0;
        } catch (Exception e) {
            exceptions.add(e);
        }

        //напишите тут ваш код

    }
}

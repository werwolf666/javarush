package com.javarush.task.task14.task1411;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/* 
User, Looser, Coder and Proger
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Person person = null;
        String[] arr = {"user", "loser", "coder", "proger"};
        String key = null;

        do {
            key = reader.readLine();
            switch (key) {
                case "user":
                    person = new Person.User();
                    doWork(person);
                    break;
                case "loser":
                    person = new Person.Loser();
                    doWork(person);
                    break;
                case "coder":
                    person = new Person.Coder();
                    doWork(person);
                    break;
                case "proger":
                    person = new Person.Proger();
                    doWork(person);
                    break;
            }
        } while (Arrays.asList(arr).contains(key));
    }

    public static void doWork(Person person) {
        if (person instanceof Person.User) {
            ((Person.User) person).live();
        }
        if (person instanceof Person.Loser) {
            ((Person.Loser) person).doNothing();
        }
        if (person instanceof Person.Coder) {
            ((Person.Coder) person).coding();
        }
        if (person instanceof Person.Proger) {
            ((Person.Proger) person).enjoy();
        }
/*
        switch (person.getClass().getSimpleName()) {
            case "User":
                ((Person.User) person).live();
                break;
            case "Loser":
                ((Person.Loser) person).doNothing();
                break;
            case "Coder":
                ((Person.Coder) person).coding();
                break;
            case "Proger":
                ((Person.Proger) person).enjoy();
                break;
        }
*/
    }
}

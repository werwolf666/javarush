package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = null;
        try {
            s = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] s1 = s.split("\\?", 0);
        String[] s2 = s1[1].split("&", 0);
        String str = "";
        String obj = "";
        for (String s3 :
                s2) {
            String[] s4 = s3.split("=", 0);
            if (!str.equals("")) {
                str += " ";
            }
            str += s4[0];
            if (s4[0].equals("obj")) {
                obj = s4[1];
            }

        }
        System.out.println(str);
        if (!obj.equals(""))
        try {
            Double d = Double.parseDouble(obj);
            alert(d);
        } catch (Exception e) {
            alert(obj);
        }

        //add your code here
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}

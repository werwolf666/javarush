package com.javarush.task.task06.task0622;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Числа по возрастанию
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] n = new int[5];
        for (int i = 0; i < 5; i++) {
            n[i] = Integer.parseInt(reader.readLine());
        }

        int tmp;

        for (int i = 0; i < 4; i++) {
            for (int j = i + 1; j < 5; j++) {
                if (n[i] > n[j]) {
                    tmp = n[i];
                    n[i] = n[j];
                    n[j] = tmp;
                }
            }
        }

        for (int i = 0; i < 5; i++) {
            System.out.println(n[i]);
        }
    }
}

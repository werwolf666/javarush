package com.javarush.task.task06.task0606;

import java.io.*;

/* 
Чётные и нечётные циферки
*/

public class Solution {

    public static int even;
    public static int odd;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        even = 0;
        odd = 0;
        do {
            if (n % 2 == 0) {
                even++;
            } else {
                odd++;
            }
            n = n / 10;
        } while (Math.abs(n) >= 1);
        System.out.println("Even: " + even + " Odd: " + odd);
        //напишите тут ваш код
    }
}

package com.javarush.task.task07.task0704;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Переверни массив
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] n = new int[10];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++) {
            n[i] = Integer.parseInt(reader.readLine());
        }
        int tmp;
        for (int i = 0; i < 5; i++) {
            tmp = n[i];
            n[i] = n[9 - i];
            n[9 - i] = tmp;
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(n[i]);
        }
        //напишите тут ваш код
    }
}


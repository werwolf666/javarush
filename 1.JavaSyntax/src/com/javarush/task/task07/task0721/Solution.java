package com.javarush.task.task07.task0721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Минимаксы в массивах
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] ar = new int[20];



        for (int i = 0; i < 20; i++) {
            ar[i] = Integer.parseInt(reader.readLine());
        }

        int maximum = ar[0];
        int minimum = ar[0];


        for (int i = 0; i < 20; i++) {
            if (ar[i] > maximum) {
                maximum = ar[i];
            }
            if (ar[i] < minimum) {
                minimum = ar[i];
            }
        }


        //напишите тут ваш код

        System.out.println(maximum);
        System.out.println(minimum);
    }
}

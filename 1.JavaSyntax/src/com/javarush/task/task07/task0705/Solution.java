package com.javarush.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Один большой массив и два маленьких
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        int[] n = new int[20];
        int[] n1 = new int[10];
        int[] n2 = new int[10];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 20; i++) {
            n[i] = Integer.parseInt(reader.readLine());
        }
        for (int i = 0; i < 10; i++) {
            n1[i] = n[i];
        }
        for (int i = 0; i < 10; i++) {
            n2[i] = n[i + 10];
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(n2[i]);
        }
        //напишите тут ваш код
    }
}

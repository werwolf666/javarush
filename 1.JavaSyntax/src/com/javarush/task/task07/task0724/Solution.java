package com.javarush.task.task07.task0724;

/* 
Семейная перепись
*/

public class Solution {
    public static void main(String[] args) {
        Human gf1 = new Human("Vova", true, 90);
        Human gm1 = new Human("Maria", false, 80);
        Human gf2 = new Human("Petr", true, 90);
        Human gm2 = new Human("Dasha", false, 80);
        Human f = new Human("Fedor", true, 30, gf1, gm1);
        Human m = new Human("Tanya", false, 28, gf2, gm2);
        Human son1 = new Human("Kolya", true, 5, f, m);
        Human son2 = new Human("Lesha", true, 7, f, m);
        Human son3 = new Human("Sema", true, 8, f, m);

        System.out.println(gf1);
        System.out.println(gm1);
        System.out.println(gf2);
        System.out.println(gm2);
        System.out.println(f);
        System.out.println(m);
        System.out.println(son1);
        System.out.println(son2);
        System.out.println(son3);
        //напишите тут ваш код
    }

    public static class Human {
        private String name;
        private boolean sex;
        private int age;
        private Human father;
        private Human mother;

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }
}























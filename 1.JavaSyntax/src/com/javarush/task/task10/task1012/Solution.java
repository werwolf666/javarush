package com.javarush.task.task10.task1012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Количество букв
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // алфавит
        String abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<Character>();
        for (int i = 0; i < abcArray.length; i++) {
            alphabet.add(abcArray[i]);
        }

        // ввод строк
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            String s = reader.readLine();
            list.add(s.toLowerCase());
        }

        long[] counter = new long[abc.length()];

        for (int i = 0; i < 10; i++) {
            String s = list.get(i);
            for (int j = 0; j < s.length(); j++) {
                int key = abc.indexOf(s.charAt(j));
                if (key >= 0) {
                    counter[key]++;
                }
            }
        }
        for (int i = 0; i < abc.length(); i++) {
            System.out.println(alphabet.get(i) + " " + counter[i]);
        }



        // напишите тут ваш код
    }

}

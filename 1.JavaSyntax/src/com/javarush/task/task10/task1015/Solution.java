package com.javarush.task.task10.task1015;

import java.util.ArrayList;

/* 
Массив списков строк
*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<String>[] arrayOfStringList = createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList() {
        ArrayList<String>[] arr = new ArrayList[2];
        ArrayList<String> list1 = new ArrayList<>();
        list1.add("1sdfas");
        list1.add("2sdfas");
        ArrayList<String> list2 = new ArrayList<>();
        list2.add("3sdfas");
        list2.add("4sdfas");
        arr[0] = list1;
        arr[1] = list2;

        //напишите тут ваш код

        return arr;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList) {
        for (ArrayList<String> list : arrayOfStringList) {
            for (String s : list) {
                System.out.println(s);
            }
        }
    }
}
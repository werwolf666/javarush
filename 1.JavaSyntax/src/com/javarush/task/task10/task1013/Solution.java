package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        private String name;
        private String lastName;
        private int age;
        private String city;
        private String country;
        private String prof;

        public Human(String name, String lastName, int age, String city, String country, String prof) {
            this.name = name;
            this.lastName = lastName;
            this.age = age;
            this.city = city;
            this.country = country;
            this.prof = prof;
        }

        public Human(String name) {
            this.name = name;
        }

        public Human(String name, String lastName) {
            this.name = name;
            this.lastName = lastName;
        }

        public Human(String name, String lastName, int age) {
            this.name = name;
            this.lastName = lastName;
            this.age = age;
        }

        public Human(String name, String lastName, int age, String city) {
            this.name = name;
            this.lastName = lastName;
            this.age = age;
            this.city = city;
        }

        public Human(String lastName, int age, String city) {
            this.lastName = lastName;
            this.age = age;
            this.city = city;
        }

        public Human(int age, String city, String country) {
            this.age = age;
            this.city = city;
            this.country = country;
        }

        public Human(int age, String city, String country, String prof) {
            this.age = age;
            this.city = city;
            this.country = country;
            this.prof = prof;
        }

        public Human(String city, String country, String prof) {
            this.city = city;
            this.country = country;
            this.prof = prof;
        }

        public Human(String name, String lastName, int age, String city, String country) {
            this.name = name;
            this.lastName = lastName;
            this.age = age;
            this.city = city;
            this.country = country;
        }
        // напишите тут ваши переменные и конструкторы
    }
}

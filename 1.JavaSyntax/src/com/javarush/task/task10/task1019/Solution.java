package com.javarush.task.task10.task1019;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Функциональности маловато!
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String, Integer> map = new HashMap<>();

        String s = "";
        do {
            s = reader.readLine();
            if (s.equals("")) break;
            int id = Integer.parseInt(s);
            s = reader.readLine();
            if (s.equals("")) break;
            map.put(s, id);
        } while (true);
        for (Map.Entry e :
                map.entrySet()) {
            System.out.println(e.getValue() + " " + e.getKey());
        }

    }
}

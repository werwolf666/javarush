package com.javarush.task.task05.task0526;

/* 
Мужчина и женщина
*/

public class Solution {
    public static void main(String[] args) {
        Man man1 = new Man("vasya", 30, "asdfasf");
        Man man2 = new Man("petya", 40, "asdfasf");
        Woman woman1 = new Woman("Lena", 30, "asdfasf");
        Woman woman2 = new Woman("Sveta", 40, "asdfasf");
                System.out.println(new Man("vasya", 30, "asdfasf"));
        System.out.println();
        System.out.println();
        System.out.println();
        //напишите тут ваш код
    }

    public static class Man {
        private  String name;
        private int age;
        private String address;

        public Man(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        public String toString() {
            return this.name + " " + this.age + " " + this.address;
        }
    }
    public static class Woman {
        private  String name;
        private int age;
        private String address;

        public Woman(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }

        public String toString() {
            return this.name + " " + this.age + " " + this.address;
        }
    }
    //напишите тут ваш код
}

package com.javarush.task.task05.task0517;

/* 
Конструируем котиков
*/

public class Cat {
    private String name = null;
    private int age = 5;
    private int weight = 10;
    private String color = "Black";
    private String address = null;

    public Cat(String name) {
        this.name = name;
    }

    public Cat(String name, int weight, int age) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public Cat(int weight, String color) {
        this.weight = weight;
        this.color = color;
    }

    public Cat(int weight, String color, String address) {
        this.weight = weight;
        this.color = color;
        this.address = address;
    }

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }
    //напишите тут ваш код


    public static void main(String[] args) {

    }
}

package com.javarush.task.task05.task0507;

/* 
Среднее арифметическое
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        int figure;
        double summa = 0;
        int count = 0;
        InputStream inputStream = System.in;
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        do {
            figure = Integer.parseInt(reader.readLine());
            summa += figure;
            count++;
        } while (figure != -1);
        System.out.println((summa + 1)/(count - 1));
        //напишите тут ваш код
    }
}


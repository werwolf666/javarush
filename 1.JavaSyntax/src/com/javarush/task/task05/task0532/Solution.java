package com.javarush.task.task05.task0532;

import java.io.*;

/* 
Задача по алгоритмам
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int maximum = 0;
        int n = Integer.parseInt(reader.readLine());
        int s = 0;
        for (int i = 0; i < n; i++) {
            s = Integer.parseInt(reader.readLine());
            if (maximum < s || i == 0) {
                maximum = s;
            }
        }

        //напишите тут ваш код

        System.out.println(maximum);
    }
}

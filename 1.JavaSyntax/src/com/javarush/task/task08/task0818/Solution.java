package com.javarush.task.task08.task0818;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Только для богачей
*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        HashMap<String, Integer> map = new HashMap();
        map.put("Stallone", 100);
        map.put("Stallone1", 200);
        map.put("Stallone2", 300);
        map.put("Stallone3", 400);
        map.put("Stallone4", 500);
        map.put("Stallone5", 600);
        map.put("Stallone6", 700);
        map.put("Stallone7", 800);
        map.put("Stallone8", 900);
        map.put("Stallone9", 1000);
        return map;
        //напишите тут ваш код
    }

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, Integer> e :
                map.entrySet()) {
            if (e.getValue() < 500) {
                list.add(e.getKey());
            }
        }
        for (String s :
                list) {
            map.remove(s);
        }
        //напишите тут ваш код
    }

    public static void main(String[] args) {

    }
}
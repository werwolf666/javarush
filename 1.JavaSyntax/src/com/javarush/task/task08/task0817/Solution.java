package com.javarush.task.task08.task0817;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* 
Нам повторы не нужны
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("Stallone", "Ivan");
        map.put("Stallone1", "Ivan1");
        map.put("Stallone2", "Ivan2");
        map.put("Stallone3", "Ivan3");
        map.put("Stallone4", "Ivan4");
        map.put("Stallone5", "Ivan5");
        map.put("Stallone6", "Ivan6");
        map.put("Stallone7", "Ivan7");
        map.put("Stallone8", "Ivan8");
        map.put("Stallone9", "Ivan9");
        //напишите тут ваш код
        return map;

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map) {
        ArrayList<String> list = new ArrayList();
        ArrayList<String> doubleList = new ArrayList();
        for (Map.Entry<String, String> e :
                map.entrySet()) {
            if (list.contains(e.getValue())) {
                doubleList.add(e.getValue());
            }
            list.add(e.getValue());
        }
        for (String s :
                doubleList) {
            removeItemFromMapByValue(map, s);
        }
        //напишите тут ваш код

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task08.task0824;

/* 
Собираем семейство
*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        ArrayList<Human> childrens = new ArrayList<>();
        Human hum1 = new Human("Elen", false, 10, new ArrayList<>());
        childrens.add(hum1);
        Human hum2 = new Human("Peter", true, 11, new ArrayList<>());
        childrens.add(hum2);
        Human hum3 = new Human("Maria", false, 10, new ArrayList<>());
        childrens.add(hum3);
        Human hum4 = new Human("Tanya", false, 30, childrens);
        Human hum5 = new Human("Sergey", true, 35, childrens);
        ArrayList<Human> childrens1 = new ArrayList<>();
        childrens1.add(hum4);
        ArrayList<Human> childrens2 = new ArrayList<>();
        childrens2.add(hum5);
        Human hum6 = new Human("Alex", true, 75, childrens1);
        Human hum7 = new Human("Tonya", false, 80, childrens1);
        Human hum8 = new Human("John", true, 75, childrens2);
        Human hum9 = new Human("Vika", false, 80, childrens2);

        System.out.println(hum1);
        System.out.println(hum2);
        System.out.println(hum3);
        System.out.println(hum4);
        System.out.println(hum5);
        System.out.println(hum6);
        System.out.println(hum7);
        System.out.println(hum8);
        System.out.println(hum9);



        //напишите тут ваш код
    }

    public static class Human {

        private String name;
        private boolean sex;
        private int age;
        private ArrayList<Human> children;

        public Human(String name, boolean sex, int age, ArrayList<Human> children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        //напишите тут ваш код

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }

}

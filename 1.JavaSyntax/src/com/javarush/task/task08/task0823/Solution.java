package com.javarush.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Омовение Рамы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str = reader.readLine();
        char[] chars = str.toCharArray();
        boolean is_space = true;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == (" ").charAt(0)) {
                is_space = true;
            } else {
                if (is_space) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    is_space = false;
                }
            }

        }

        for (int i = 0; i < chars.length; i++) {
            System.out.print(chars[i]);
//            System.out.println("");
        }

        //напишите тут ваш код
    }
}

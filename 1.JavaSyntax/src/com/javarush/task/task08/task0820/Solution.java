package com.javarush.task.task08.task0820;

import java.util.HashSet;
import java.util.Set;

/* 
Множество всех животных
*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();
        Set<Dog> dogs = createDogs();

        Set<Object> pets = join(cats, dogs);
        printPets(pets);

        removeCats(pets, cats);
        printPets(pets);
    }

    public static Set<Cat> createCats() {
        HashSet<Cat> result = new HashSet<Cat>();
        Cat cat1 = new Cat();
        result.add(cat1);
        Cat cat2 = new Cat();
        result.add(cat2);
        Cat cat3 = new Cat();
        result.add(cat3);
        Cat cat4 = new Cat();
        result.add(cat4);

        //напишите тут ваш код

        return result;
    }

    public static Set<Dog> createDogs() {
        HashSet<Dog> result = new HashSet<Dog>();
        Dog dog1 = new Dog();
        result.add(dog1);
        Dog dog2 = new Dog();
        result.add(dog2);
        Dog dog3 = new Dog();
        result.add(dog3);

        //напишите тут ваш код

        return result;
    }

    public static Set<Object> join(Set<Cat> cats, Set<Dog> dogs) {
        HashSet<Object> result = new HashSet<>();
        result.addAll(cats);
        result.addAll(dogs);
        //напишите тут ваш код
        return result;
    }

    public static void removeCats(Set<Object> pets, Set<Cat> cats) {
        pets.removeIf(p -> p.getClass().getName().contains("Cat"));
        //напишите тут ваш код
    }

    public static void printPets(Set<Object> pets) {
        for (Object pet :
                pets) {
            System.out.println(pet);
        }
        //напишите тут ваш код
    }

    public static class Cat {

    }

    public static class Dog {

    }

    //напишите тут ваш код
}

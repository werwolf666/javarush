package com.javarush.task.task08.task0827;

import java.util.Date;

/* 
Работа с датой
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(isDateOdd("JAN 5 2013"));
    }

    public static boolean isDateOdd(String date) {
        Date yearStartTime = new Date(date);
        yearStartTime.setHours(0);
        yearStartTime.setMinutes(0);
        yearStartTime.setSeconds(0);
        yearStartTime.setDate(1);      // первое число
        yearStartTime.setMonth(0);

        Date date1 = new Date(date);
        int days = 1 + (int) ((date1.getTime() - yearStartTime.getTime()) / 1000 / 3600 / 24);
        if (days % 2 == 0) {
            return false;
        } else{
            return true;
        }
    }
}

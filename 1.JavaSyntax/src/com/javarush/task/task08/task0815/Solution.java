package com.javarush.task.task08.task0815;

import org.w3c.dom.Entity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* 
Перепись населения
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("Иванов", "Иван");
        map.put("Иванов1", "Иван");
        map.put("Иванов2", "Иван");
        map.put("Иванов3", "Иван");
        map.put("Иванов4", "Иван");
        map.put("Иванов5", "Иван");
        map.put("Иванов6", "Иван");
        map.put("Иванов7", "Иван");
        map.put("Иванов8", "Иван");
        map.put("Иванов9", "Иван");
        return map;
        //напишите тут ваш код

    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name) {
        int count = 0;
        for ( Map.Entry e :
                map.entrySet()) {
            if (e.getValue() == name) {
                count++;
            }
        }
        return count;
        //напишите тут ваш код

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName) {
        int count = 0;
        for ( Map.Entry e :
                map.entrySet()) {
            if (e.getKey() == lastName) {
                count++;
            }
        }
        return count;
        //напишите тут ваш код

    }

    public static void main(String[] args) {

    }
}

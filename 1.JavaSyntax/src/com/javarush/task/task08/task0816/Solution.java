package com.javarush.task.task08.task0816;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/* 
Добрая Зинаида и летние каникулы
*/

public class Solution {
    public static HashMap<String, Date> createMap() {
        HashMap<String, Date> map = new HashMap();
        map.put("Stallone", new Date(1975, 10, 1));
        map.put("Stallone1", new Date(1975, 1, 1));
        map.put("Stallone2", new Date(1975, 6, 1));
        map.put("Stallone3", new Date(1975, 10, 1));
        map.put("Stallone4", new Date(2005, 12, 2));
        map.put("Stallone5", new Date(1975, 7, 1));
        map.put("Stallone6", new Date(1975, 10, 1));
        map.put("Stallone7", new Date(1975, 10, 1));
        map.put("Stallone8", new Date(1975, 8, 1));
        map.put("Stallone9", new Date(1975, 10, 1));
        return map;

        //напишите тут ваш код
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, Date> e :
                map.entrySet()) {
            if (e.getValue().getMonth() > 4 && e.getValue().getMonth() < 8) {
                list.add(e.getKey());
            }
        }
        for (String s :
                list) {
            map.remove(s);
        }
    }

    public static void main(String[] args) {
    }
}

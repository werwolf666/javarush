package com.javarush.task.task08.task0814;

import java.util.HashSet;
import java.util.Set;

/* 
Больше 10? Вы нам не подходите
*/

public class Solution {
    public static HashSet<Integer> createSet() {
        HashSet<Integer> set = new HashSet();
        for (int i = 0; i < 20; i++) {
            set.add(i);
        }
        return set;
        //напишите тут ваш код

    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set) {
        HashSet<Integer> bad = new HashSet();
        for (Integer i :
                set) {
            if (i > 10) {
                bad.add(i);
            }
        }
        for (Integer i :
                bad) {
            set.remove(i);
        }
        return set;
        //напишите тут ваш код

    }

    public static void main(String[] args) {

    }
}

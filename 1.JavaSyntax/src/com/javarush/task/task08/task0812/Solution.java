package com.javarush.task.task08.task0812;

import java.io.*;
import java.util.ArrayList;

/* 
Cамая длинная последовательность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        ArrayList<Integer> list = new ArrayList();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for (int i = 0; i < 10; i++) {
            list.add(Integer.parseInt(reader.readLine()));
        }

        int length = 0;
        int maxLength = 0;
        int lastFigure = 0;
        for (int i = 0; i < 10; i++) {
            if (i == 0) {
                lastFigure = list.get(i);
            }
            if (lastFigure == list.get(i)) {
                length++;
            } else {
                lastFigure = list.get(i);
                if (length > maxLength) {
                    maxLength = length;
                }
                length = 1;
            }
        }
        if (length > maxLength) {
            maxLength = length;
        }
        System.out.println(maxLength);
        //напишите тут ваш код

    }
}
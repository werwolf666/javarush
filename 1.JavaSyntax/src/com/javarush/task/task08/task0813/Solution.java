package com.javarush.task.task08.task0813;

import java.util.HashSet;
import java.util.Set;

/* 
20 слов на букву «Л»
*/

public class Solution {
    public static HashSet<String> createSet() {
        HashSet<String> set = new HashSet();
        set.add("Лук1");
        set.add("Лук2");
        set.add("Лук3");
        set.add("Лук4");
        set.add("Лук5");
        set.add("Лук6");
        set.add("Лук7");
        set.add("Лук8");
        set.add("Лук9");
        set.add("Лук10");
        set.add("Лук11");
        set.add("Лук12");
        set.add("Лук13");
        set.add("Лук14");
        set.add("Лук15");
        set.add("Лук16");
        set.add("Лук17");
        set.add("Лук18");
        set.add("Лук19");
        set.add("Лук20");
        return set;
    }

    public static void main(String[] args) {

    }
}

package com.javarush.task.task08.task0821;

import java.util.HashMap;
import java.util.Map;

/* 
Однофамильцы и тёзки
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = createPeopleList();
        printPeopleList(map);
    }

    public static Map<String, String> createPeopleList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("Stallone", "Rembo1");
        map.put("Stallone", "Rembo2");
        map.put("Stallone", "Rembo3");
        map.put("Stallone", "Rembo4");
        map.put("Stallone", "Rembo5");
        map.put("Stallone1", "Rembo");
        map.put("Stallone2", "Rembo");
        map.put("Stallone3", "Rembo");
        map.put("Stallone4", "Rembo");
        map.put("Stallone5", "Rembo");

        //напишите тут ваш код

        return map;
    }

    public static void printPeopleList(Map<String, String> map) {
        for (Map.Entry<String, String> s : map.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }
}

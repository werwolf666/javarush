package com.javarush.task.task09.task0922;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* 
Какое сегодня число?
*/

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String date_str = reader.readLine();
        Date date = new Date(date_str);
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, Y", Locale.ENGLISH);
        System.out.println(format.format(date).toUpperCase());
        //напишите тут ваш код
    }
}

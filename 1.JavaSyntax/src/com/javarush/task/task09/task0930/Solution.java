package com.javarush.task.task09.task0930;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Задача по алгоритмам
*/

public class Solution {
    public static void main(String[] args) throws Exception {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<String>();
        while (true) {
            String s = reader.readLine();
            if (s.isEmpty()) break;
            list.add(s);
        }

        String[] array = list.toArray(new String[list.size()]);

//        String[] array = new String[] {"sadfs", "3", "bdf", "1"};
        sort(array);

        for (String x : array) {
            System.out.println(x);
        }
    }

    public static void sort(String[] array) {
        ArrayList<String> strings = new ArrayList<>();
        ArrayList<Integer> numbers = new ArrayList<>();
        int slen =0;
        int nlen =0;
        for (int i = 0; i < array.length; i++) {
            if (isNumber(array[i])) {
                numbers.add(Integer.parseInt(array[i]));
                nlen++;
            } else {
                strings.add(array[i]);
                slen++;
            }
        }
        for (int i = numbers.size() - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (numbers.get(j) < numbers.get(j + 1)) {
                    int t = numbers.get(j);
                    numbers.set(j, numbers.get(j + 1));
                    numbers.set(j + 1, t);
                }
            }
        }
        for (int i = strings.size() - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (isGreaterThan(strings.get(j), strings.get(j + 1))) {
                    String t = strings.get(j);
                    strings.set(j, strings.get(j + 1));
                    strings.set(j + 1, t);
                }
            }
        }

        String[] newarray = new String[array.length];
        slen = 0;
        nlen = 0;
        for (int i = 0; i < array.length; i++) {
            if (isNumber(array[i])) {
                newarray[i] = String.valueOf(numbers.get(nlen));
                nlen++;
            } else {
                newarray[i] = String.valueOf(strings.get(slen));
                slen++;
            }
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = newarray[i];
        }
        //напишите тут ваш код
    }

    // Метод для сравнения строк: 'а' больше чем 'b'
    public static boolean isGreaterThan(String a, String b) {
        return a.compareTo(b) > 0;
    }


    // Переданная строка - это число?
    public static boolean isNumber(String s) {
        if (s.length() == 0) return false;

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if ((i != 0 && c == '-') // есть '-' внутри строки
                    || (!Character.isDigit(c) && c != '-')) // не цифра и не начинается с '-'
            {
                return false;
            }
        }
        return true;
    }
}

package com.javarush.task.task09.task0927;

import java.util.*;

/* 
Десять котов
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap() {
        HashMap<String, Cat> map = new HashMap<String, Cat>();
        Cat cat1 = new Cat("Barsik1");
        map.put(cat1.name, cat1);
        Cat cat2 = new Cat("Barsik2");
        map.put(cat2.name, cat2);
        Cat cat3 = new Cat("Barsik3");
        map.put(cat3.name, cat3);
        Cat cat4 = new Cat("Barsik4");
        map.put(cat4.name, cat4);
        Cat cat5 = new Cat("Barsik5");
        map.put(cat5.name, cat5);
        Cat cat6 = new Cat("Barsik6");
        map.put(cat6.name, cat6);
        Cat cat7 = new Cat("Barsik7");
        map.put(cat7.name, cat7);
        Cat cat8 = new Cat("Barsik8");
        map.put(cat8.name, cat8);
        Cat cat9 = new Cat("Barsik9");
        map.put(cat9.name, cat9);
        Cat cat10 = new Cat("Barsik10");
        map.put(cat10.name, cat10);
        return map;
        //напишите тут ваш код
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map) {
        Set<Cat> set1 = new HashSet<>();
        for (Map.Entry<String, Cat> cat :
                map.entrySet()) {
            set1.add(cat.getValue());
        }
        return set1;
        //напишите тут ваш код
    }

    public static void printCatSet(Set<Cat> set) {
        for (Cat cat : set) {
            System.out.println(cat);
        }
    }

    public static class Cat {
        private String name;

        public Cat(String name) {
            this.name = name;
        }

        public String toString() {
            return "Cat " + this.name;
        }
    }


}

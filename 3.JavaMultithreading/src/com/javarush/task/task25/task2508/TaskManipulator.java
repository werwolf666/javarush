package com.javarush.task.task25.task2508;

public class TaskManipulator implements Runnable, CustomThreadManipulator {
    private Thread thread;

    @Override
    public void run() {
        while (!this.thread.isInterrupted()) {
            try {
                System.out.println(this.thread.getName());
                Thread.sleep(100);
            } catch (InterruptedException e) {
                break;
            }

        }

    }

    @Override
    public void start(String threadName) {
        this.thread = new Thread(this);
        this.thread.setName(threadName);
        this.thread.start();
    }

    @Override
    public void stop() {
        this.thread.interrupt();
//        System.out.println(this.thread.getState());
/*
        while (this.thread.getState() == Thread.State.WAITING) {

        }
*/
    }
}

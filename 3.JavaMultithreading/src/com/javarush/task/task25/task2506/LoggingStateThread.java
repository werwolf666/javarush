package com.javarush.task.task25.task2506;

public class LoggingStateThread extends Thread {
    private Thread thread;
    private State state = null;
    public LoggingStateThread(Thread thread) {
        this.thread = thread;
    }

    @Override
    public void run() {
        while (thread.getState() != State.TERMINATED) {
            if (thread.getState() != state) {
                state = thread.getState();
                System.out.println(state);
            }
        }
    }
}

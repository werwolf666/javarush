package com.javarush.task.task25.task2512;

import java.util.ArrayList;

/*
Живем своим умом
*/
public class Solution implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        t.interrupt();
        ArrayList<String> list = new ArrayList<>();
        System.out.println(e.fillInStackTrace() );
/*
        while (e != null) {
            list.add(e.toString());
            e = e.getCause();
        }
        for (int i = list.size() - 1; i > -1; i--) {
            System.out.println(list.get(i));
        }
*/

    }

    public static void main(String[] args) throws Exception {
        Solution solution = new Solution();
        solution.uncaughtException(Thread.currentThread(), new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI"))));
    }
}

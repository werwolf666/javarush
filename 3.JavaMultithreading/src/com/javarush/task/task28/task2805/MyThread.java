package com.javarush.task.task28.task2805;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by alex.vol on 19.06.2017.
 */
public class MyThread extends Thread {
    public static AtomicInteger lastPriority = new AtomicInteger(MIN_PRIORITY);

    public MyThread() {
    }

    public MyThread(Runnable target) {
        super(target);
    }

    public MyThread(ThreadGroup group, Runnable target) {
        super(group, target);
    }

    public MyThread(String name) {
        super(name);
    }

    public MyThread(ThreadGroup group, String name) {
        super(group, name);
    }

    public MyThread(Runnable target, String name) {
        super(target, name);
    }

    public MyThread(ThreadGroup group, Runnable target, String name) {
        super(group, target, name);
    }

    public MyThread(ThreadGroup group, Runnable target, String name, long stackSize) {
        super(group, target, name, stackSize);
    }

    {
        ThreadGroup threadGroup = this.getThreadGroup();
        int groupPriority = threadGroup.getMaxPriority();
        if (groupPriority < lastPriority.addAndGet(0)) {
            this.setPriority(groupPriority);
        } else {
            this.setPriority(lastPriority.addAndGet(0));
        }
        if (lastPriority.addAndGet(0) == MAX_PRIORITY) {
            lastPriority.set(MIN_PRIORITY);
        } else {
            lastPriority.addAndGet(1);
        }
    }
}

package com.javarush.task.task28.task2802;


import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* 
Пишем свою ThreadFactory
*/
public class Solution {

    public static class AmigoThreadFactory implements ThreadFactory{
        public static AtomicInteger fabricCounter = new AtomicInteger(0);
        private int fabricNumber;
        public AtomicInteger threadNumber = new AtomicInteger(0);

        public AmigoThreadFactory() {
            this.fabricNumber = this.fabricCounter.addAndGet(1);
        }

        @Override
        public Thread newThread(Runnable r) {
            ThreadGroup group = Thread.currentThread().getThreadGroup();
            int threadNumber = this.threadNumber.addAndGet(1);

            String threadName = group.getName() + "-pool-" + fabricNumber + "-thread-" + threadNumber;
            Thread t = new Thread(r, threadName);
            t.setDaemon(false);
            t.setPriority(5);
            return t;
        }
    }

    public static void main(String[] args) {
        class EmulateThreadFactoryTask implements Runnable {
            @Override
            public void run() {
                emulateThreadFactory();
            }
        }

        ThreadGroup group = new ThreadGroup("firstGroup");
        Thread thread = new Thread(group, new EmulateThreadFactoryTask());

        ThreadGroup group2 = new ThreadGroup("secondGroup");
        Thread thread2 = new Thread(group2, new EmulateThreadFactoryTask());

        thread.start();
        thread2.start();
    }

    private static void emulateThreadFactory() {
        AmigoThreadFactory factory = new AmigoThreadFactory();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        };
        factory.newThread(r).start();
        factory.newThread(r).start();
        factory.newThread(r).start();
    }
}

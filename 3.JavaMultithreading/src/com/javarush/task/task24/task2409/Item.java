package com.javarush.task.task24.task2409;

/**
 * Created by wolf on 11.06.2017.
 */
public interface Item {
    int getId();
    double getPrice();
    String getTM();
}

package com.javarush.task.task29.task2909.human;

import java.util.ArrayList;
import java.util.List;

public class University {
    private List<Student> students = new ArrayList<Student>();
    private int age;
    private String name;

    public University(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public String getName() {
        return this.name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public Student getStudentWithAverageGrade(double grade) {
        for (Student student :
                students) {
            if (student.getAverageGrade() == grade) {
                return student;
            }
        }
        return null;
    }

    public Student getStudentWithMaxAverageGrade() {
        Student maxStudent = null;
        for (Student student :
                students) {
            if (maxStudent == null || student.getAverageGrade() > maxStudent.getAverageGrade()) {
                maxStudent = student;
            }
        }
        return maxStudent;
    }

    public Student getStudentWithMinAverageGrade() {
        Student minStudent = null;
        for (Student student :
                students) {
            if (minStudent == null || student.getAverageGrade() < minStudent.getAverageGrade()) {
                minStudent = student;
            }
        }
        return minStudent;
    }

    public void expel(Student student) {
        students.remove(student);
    }

}
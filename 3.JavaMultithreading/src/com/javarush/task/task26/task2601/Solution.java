package com.javarush.task.task26.task2601;

import java.util.Arrays;
import java.util.Comparator;

/*
Почитать в инете про медиану выборки
*/
public class Solution {

    public static void main(String[] args) {
        Integer[] arr = {13, 8, 15, 5, 14, 17};
//        Integer[] arr = {};
        arr = sort(arr);
    }

    public static Integer[] sort(Integer[] array) {

        if (array.length == 0) return array;
        int mediana;

        Arrays.sort(array);

        if (array.length % 2 == 0) {
            mediana = (array[array.length / 2] + array[(array.length / 2) - 1]) / 2;
        } else {
            mediana = array[(array.length - 1) / 2];
        }

        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return (int) Math.abs(mediana - o1) - Math.abs(mediana - o2);
            }
        };

        Arrays.sort(array, comparator);

        return array;
    }
}

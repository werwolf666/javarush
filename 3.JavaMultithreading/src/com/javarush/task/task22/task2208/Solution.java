package com.javarush.task.task22.task2208;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* 
Формируем WHERE
*/
public class Solution {
    public static void main(String[] args) {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("name", "Ivanov");
        map.put("country", "Ukraine");
        map.put("city", "Kiev");
        map.put("age", null);
        System.out.println(getQuery(map));
    }
    public static String getQuery(Map<String, String> params) {
        StringBuilder s = new StringBuilder("");
        for (Map.Entry<String, String> e :
                params.entrySet()) {
            if (e.getValue() != null) {
                if (!s.toString().equals("")) {
                    s.append(" and ");
                }
                s.append(e.getKey() + " = '" + e.getValue() + "'");
            }
        }
        return s.toString();
    }
}

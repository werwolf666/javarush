package com.javarush.task.task22.task2209;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/*
Составить цепочку слов
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        //...
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader readFile = new BufferedReader(new FileReader(reader.readLine()));
        String[] file;
        String buff;
        String s = "";
        while ((buff = readFile.readLine()) != null){
            s += buff + " ";
        }
        file = s.split(" ");
        StringBuilder result = getLine(file);
        System.out.println(result.toString());
    }
    public static StringBuilder getLine(String... words) {
        Arrays.sort(words);
        StringBuilder sb = new StringBuilder();
        ArrayList<String> list = new ArrayList<>();
        if(words.length == 0 || words == null)return sb;
        for (int i = 0; i < words.length; i++){
            if(list.size()>0)break;
            for (int j = 0; j < words.length; j++){
                if(words[i].toLowerCase().charAt(words[i].length()-1) == words[j].toLowerCase().charAt(0)){
                    list.add(words[i]);
                    list.add(words[j]);
                    break;
                }
            }
        }
        for (int i = 0; i < words.length; i++) {
            if(!(list.contains(words[i])) && list.get(list.size()-1).toLowerCase().charAt(list.get(list.size()-1).length()-1) == words[i].toLowerCase().charAt(0)){
                list.add(words[i]);
            }
        }
        for(String s: words){
            if(!(list.contains(s))){
                list.add(s);
            }
        }
        for (String s : list){
            sb.append(s + " ");
        }
        sb.replace(sb.lastIndexOf(" "), sb.length()-1, "");
        return sb;
    }
}
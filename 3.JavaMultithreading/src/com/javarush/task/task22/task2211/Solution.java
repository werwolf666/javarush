package com.javarush.task.task22.task2211;

import java.io.*;
import java.util.Scanner;

/* 
Смена кодировки
*/
public class Solution {
    static String win1251TestString = "РќР°СЂСѓС€РµРЅРёРµ РєРѕРґРёСЂРѕРІРєРё РєРѕРЅСЃРѕР»Рё?"; //only for your testing

    public static void main(String[] args) throws IOException {
        String fileIn = args[0];
        String fileOut = args[1];
        FileInputStream inputStream = new FileInputStream(fileIn);
        FileOutputStream outputStream = new FileOutputStream(fileOut);
        byte[] buffer = new byte[inputStream.available()];
        inputStream.read(buffer);
        String s = new String(buffer, "UTF-8");
        buffer = s.getBytes("Windows-1251");
        outputStream.write(buffer);
        inputStream.close();
        outputStream.close();
    }
}

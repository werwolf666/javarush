package com.javarush.task.task22.task2202;

/* 
Найти подстроку
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getPartOfString("JavaRush - лучший"));
//        System.out.println(getPartOfString("JavaRush - лучший сервис обучения Java."));
    }

    public static String getPartOfString(String string) {
        try {
            int begIndex = string.indexOf(" ");
            int endIndex = begIndex;
            for (int i = 0; i < 3; i++) {
                endIndex = string.indexOf(" ", endIndex + 1);
                if (endIndex == -1) {
                    throw new TooShortStringException();
                }
            }
            int lastIndex = string.lastIndexOf(" ");
            if (lastIndex > endIndex) {
                endIndex = string.indexOf(" ", endIndex + 1);
            } else {
                endIndex = string.length();
            }
            return string.substring(begIndex + 1, endIndex);
        } catch (Exception e) {
            throw new TooShortStringException();
        }
    }

    public static class TooShortStringException extends RuntimeException {
    }
}

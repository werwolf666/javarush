package com.javarush.task.task22.task2201;

public class TooShortStringSecondThreadException extends RuntimeException {
    private Throwable cause;
    public TooShortStringSecondThreadException(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }
}

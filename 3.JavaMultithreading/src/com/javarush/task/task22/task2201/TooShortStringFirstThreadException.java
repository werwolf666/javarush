package com.javarush.task.task22.task2201;

public class TooShortStringFirstThreadException extends RuntimeException {
    private Throwable cause;
    public TooShortStringFirstThreadException(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public synchronized Throwable getCause() {
        return cause;
    }
}

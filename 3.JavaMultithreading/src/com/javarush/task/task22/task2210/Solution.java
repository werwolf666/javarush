package com.javarush.task.task22.task2210;

import java.util.ArrayList;
import java.util.StringTokenizer;

/*
StringTokenizer
*/
public class Solution {
    public static void main(String[] args) {
        String[] res = getTokens("level22.lesson13.task01", ".");
    }
    public static String [] getTokens(String query, String delimiter) {
        StringTokenizer t = new StringTokenizer(query, delimiter);
        ArrayList<String> list = new ArrayList<>();
        while (t.hasMoreElements()) {
            list.add((String) t.nextElement());
        }
        String[] res = list.toArray(new String[0]);
        return res;
    }
}

package com.javarush.task.task22.task2207;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/* 
Обращенные слова
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        Scanner scanner = new Scanner(new File(filename));
//        FileReader fileReader = new FileReader(filename);
        String s;
        ArrayList<String> arrayList = new ArrayList<>();
        while (scanner.hasNext()) {
            s = scanner.nextLine();
            arrayList.addAll(Arrays.asList(s.split(" ")));
        }
        ArrayList<Integer> done = new ArrayList();
        for (int i = 0; i < arrayList.size(); i++) {
            String s1 = arrayList.get(i);
            for (int j = i + 1; j < arrayList.size(); j++) {
                StringBuilder s3 = new StringBuilder(arrayList.get(j));
                if (arrayList.get(i).equals(s3.reverse().toString()) && !done.contains(i) && !done.contains(j)) {
                    done.add(i);
                    done.add(j);
                    Pair pair = new Pair();
                    pair.first = s1;
                    pair.second = s3.reverse().toString();
                    result.add(pair);

                }
            }
        }
    }

    public static class Pair {
        String first;
        String second;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            return second != null ? second.equals(pair.second) : pair.second == null;

        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}

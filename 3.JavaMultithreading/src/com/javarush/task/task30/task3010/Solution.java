package com.javarush.task.task30.task3010;

/* 
Минимальное допустимое основание системы счисления
*/

import java.math.BigDecimal;
import java.math.BigInteger;

public class Solution {
    public static void main(String[] args) {
        try {
            String s = args[0];
            int radix = 0;
            for (int i = 35; i > 0; i--) {
                char c;
                if (i < 10) {
                    c = String.valueOf(i).charAt(0);
                } else {
                    c = (char) (55 + i);
                }
                if (s.toUpperCase().indexOf(c) != -1) {
                   radix = i + 1;
                   break;
                }
            }
            if (radix < 2) {
                radix = 2;
            }
            BigInteger bi = new BigInteger(s,radix);
            System.out.println(radix);
        } catch (Exception e) {
            System.out.println("incorrect");
        }
    }
}
package com.javarush.task.task30.task3008.client;

import com.javarush.task.task30.task3008.ConsoleHelper;
import com.javarush.task.task30.task3008.Message;
import com.javarush.task.task30.task3008.MessageType;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by wolf on 18.06.2017.
 */
public class BotClient extends Client {
    public class BotSocketThread extends Client.SocketThread {
        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }

        @Override
        protected void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
            if (!message.isEmpty() && message.contains(": ")) {
                String[] arr = message.split(": ");
                if (arr.length == 2) {
                    SimpleDateFormat df;
                    switch (arr[1]) {
                        case "дата":
                            df = new SimpleDateFormat("d.MM.YYYY");
                            break;
                        case "день":
                            df = new SimpleDateFormat("d");
                            break;
                        case "месяц":
                            df = new SimpleDateFormat("MMMM");
                            break;
                        case "год":
                            df = new SimpleDateFormat("YYYY");
                            break;
                        case "время":
                            df = new SimpleDateFormat("H:mm:ss");
                            break;
                        case "час":
                            df = new SimpleDateFormat("H");
                            break;
                        case "минуты":
                            df = new SimpleDateFormat("mm");
                            break;
                        case "секунды":
                            df = new SimpleDateFormat("ss");
                            break;
                        default:
                            df = null;
                    }
                    if (df != null) {
                        sendTextMessage("Информация для " + arr[0] + ": " + df.format(Calendar.getInstance().getTime()));
                    }
                }

            }
        }
    }

    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    @Override
    protected String getUserName() {
        return "date_bot_" + (int) (100 * Math.random());
    }

    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    public static void main(String[] args) {
        new BotClient().run();
    }
}

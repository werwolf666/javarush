package com.javarush.task.task30.task3008.client;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by wolf on 18.06.2017.
 */
public class ClientGuiModel {
    private final Set<String> allUserNames = new HashSet<>();

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }

    public String getNewMessage() {

        return newMessage;
    }

    private String newMessage;

    public Set<String> getAllUserNames() {
        return Collections.synchronizedSet(allUserNames);
    }

    public void addUser(String newUserName) {
        allUserNames.add(newUserName);
    }

    public void deleteUser(String userName) {
        allUserNames.remove(userName);
    }
}

package com.javarush.task.task21.task2104;

import java.util.HashSet;
import java.util.Set;

/* 
Equals and HashCode
*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public boolean equals(Object n) {
        if (this == n) return true;
        if (!(n instanceof Solution)) return false;
        if (n == null || n.getClass() != this.getClass()) return false;
        if (n.hashCode() != this.hashCode()) return false;

        Solution guest = (Solution) n;
        return (first == guest.first || (first != null &&first.equals(guest.first)))
                && (last == guest.last || (last != null && last .equals(guest.last)
        ));

/*
        if (n.first == null && this.first != null) return false;
        if (n.last == null && this.last != null) return false;
        if (n.first != null && this.first == null) return false;
        if (n.last != null && this.last == null) return false;
        if (n.first == null && n.last == null && this.first == null && this.last == null) return true;
//        if (this.first == n.first && this.last == n.last) return true;
        return n.first.equals(first) && n.last.equals(last);
*/
    }

    public int hashCode() {
        int resultat = first != null ? first.hashCode() : 0;
        resultat = 31 * resultat + (last != null ? last.hashCode() : 0);
        return resultat;
//        return first.hashCode() + last.hashCode();
    }

    public static void main(String[] args) {
        Set<Solution> s = new HashSet<>();
        Solution a = new Solution("Donald", "Duck");
        Solution b = new Solution("Donald", "Duck");
        System.out.println(a.equals(b));


        s.add(new Solution("Donald", "Duck"));
        System.out.println(s.contains(new Solution("Donald", "Duck")));
    }
}

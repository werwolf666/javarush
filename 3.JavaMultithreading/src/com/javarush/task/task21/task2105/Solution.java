package com.javarush.task.task21.task2105;

import java.util.HashSet;
import java.util.Set;

/* 
Исправить ошибку. Сравнение объектов
*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Solution))
            return false;
        Solution guest = (Solution) o;
        return (first == guest.first || (first != null &&first.equals(guest.first)))
                && (last == guest.last || (last != null && last .equals(guest.last)
        ));
    }

    public int hashCode() {
        int resultat = first != null ? first.hashCode() : 0;
        resultat = 31 * resultat + (last != null ? last.hashCode() : 0);
        return resultat;
    }

    public static void main(String[] args) {
        Set<Solution> s = new HashSet<>();

        Solution a = new Solution("Donald", "Duck");
        Solution b = new Solution("Donald", "Duck");
        System.out.println(a.equals(b));


        s.add(new Solution("Mickey", "Mouse"));
        System.out.println(s.contains(new Solution("Mickey", "Mouse")));
    }
}

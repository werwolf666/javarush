package com.javarush.task.task21.task2113;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex.vol on 02.06.2017.
 */
public class Hippodrome {

    private List<Horse> horses;

    public static Hippodrome game;

    public List<Horse> getHorses() {
        return horses;
    }

    public Hippodrome(List<Horse> horses) {
        this.horses = horses;
    }

    public static void main(String[] args) throws InterruptedException {
        game = new Hippodrome(new ArrayList<Horse>());
        game.getHorses().add(new Horse("Zorka", 3, 0));
        game.getHorses().add(new Horse("Murka", 3, 0));
        game.getHorses().add(new Horse("Shmurka", 3, 0));
        game.run();
        game.printWinner();
    }

    public void run() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            move();
            print();
            Thread.sleep(200);
        }

    }

    public Horse getWinner() {
        double max = 0;
        Horse winner = null;
        for (Horse horse :
                horses) {
            if (horse.getDistance() > max) {
                winner = horse;
                max = horse.getDistance();
            }
        }
        return winner;
    }

    public void printWinner() {
        System.out.println("Winner is " + this.getWinner().getName() + "!");
    }

    public void move() {
        for (Horse horse :
                horses) {
            horse.move();
        }

    }

    public void print() {
        for (Horse horse :
                horses) {
            horse.print();
        }
        for (int i = 0; i < 10; i++) {
            System.out.println();
        }

    }

}
